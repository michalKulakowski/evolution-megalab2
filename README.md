# evolution-megalab2

## Complete configuration
Go to /config and fill both config files:
* /config/app.config.json - that is config for front end.
* /config/mysql.config.php - mysql config to connect to DB

### app.config.json
```
{
  "googleMapApiKey": "API KEY TO GOOGLE MAPS",
  "appBaseUrl": "",
  "enableHTTPSRedirect": false
}
```
**appBaseUrl** - if you have domain which is pointed to public folder then this url will looks like: "https://yourdomain.com"
But if from some reasone your domain isn't pointed to public then: "https://yourdomain.com/path/to/public"
Remember to specify that you are using HTTP or HTTPS protocol.

**enableHTTPSRedirect** - if you are using HTTPS protocol then that flag will redirect all request to front-end to secure request.
 
### webpack config
```
- vue.config.js

publicPath: process.env.NODE_ENV === 'production'
    ? '/science/s317/evolution-megalab/public'
    : '/'
```
If your domain isn't pointed to public folder then here you need to specify path to it. Webpack will add this automatically before including any files.
In example you can see that only production required a different path.
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
After runing build you should have **dist** folder in your main directory. That will be your new public folder.
You need to remove old **public** folder and rename **dist** folder.
From deploy perspective you can remove us well folders:
* build
* node_modules
* src

They are required only in development process.
