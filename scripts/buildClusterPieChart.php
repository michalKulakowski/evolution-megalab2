<?php
require __DIR__ . '/../app.php';

$clusterModel = new \Model\Cluster();
$eventModel = new \Model\Event();

$clusters = $clusterModel->query('SELECT * FROM map_cluster');

foreach ($clusters as $cluster){
    echo $cluster->id ."\n";
    $eventIds = $clusterModel->getEventIds($cluster->id);
    $eventCount = count($eventIds);
    $params = array (
        'yellow' => 0,
        'pink'   => 0,
        'brown'  => 0,
        'other'  => 0
    );
    foreach ($eventIds as $eventId){
        $val = $eventModel->countAllColourSpotsPercentage($eventId);
        $params['yellow'] += $val['yellow'] / $eventCount;
        $params['pink'] += $val['pink'] / $eventCount;
        $params['brown'] += $val['brown'] / $eventCount;
    }
    if(array_sum($params) == 0){
        echo "  empty\n";
        continue;
    }

    \Library\Pins\Images::createPieChart($params, true, CLUSTER_DIR.'/'.$clusterModel->getImageName($cluster), true);
}

