<?php
require __DIR__ . '/../app.php';

$eventsModel = new \Model\Event();

//$res = $eventsModel->getEvents();
//If you want to generate pins for all events you can use:
 $res = $eventsModel->query('SELECT id as event_id FROM event');

foreach ($res as $event)
{
    echo $event->event_id ."\n";
    $count = $eventsModel->countAllColourSpotsPercentage($event->event_id);

    if(array_sum($count) == 0){
        echo "  empty\n";
        continue;
    }
    $fileName = '/pin'.$event->event_id.'.gif';
    \Library\Pins\Images::createPieChartPin($count, true, true, PINS_DIR.$fileName);
}