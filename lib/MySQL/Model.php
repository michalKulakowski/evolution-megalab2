<?php
namespace Library\MySQL;

class Model
{
    public function getOne($query)
    {
        $result = $this->_query($query);
        if($result)
         return $result->fetch_object();
    }

    public function insert($table, $data)
    {
        $columns = '';
        $values = '';

        foreach ($data as $column => $val){
            if(!empty($columns)){
                $columns .= ', ';
                $values .= ', ';
            }
            $columns .= $column;
            $values .= '"'.$this->_escape($val).'"';
        }

        $query = 'INSERT INTO '.$table.' ('.$columns.') VALUES ('.$values.')';

        $this->_query($query);

        return $this->_getConnection()->insert_id;
    }

    public function delete($query)
    {
        return $this->_query($query);
    }

    public function startTransaction()
    {
        $this->_getConnection()->autocommit(FALSE);
    }

    public function rollback()
    {
        $this->_getConnection()->rollback();
    }

    public function commit()
    {
        if(!$this->_getConnection()->commit())
            throw new \Exception('Transaction commit failed');

        return true;
    }

    /**
     * @param $query
     * @return bool|\mysqli_result
     * @throws \Exception
     */
    protected function _query($query)
    {
        $result = $this->_getConnection()->query($query);

        if(!$result){
            $errorMsg 	= $this->_getConnection()->error;
            $errNo 		= $this->_getConnection()->errno;

            throw new \Exception($errorMsg,$errNo);
        }

        return $result;
    }

    public function query($query, $params = null)
    {

        $result = $this->_query($query);

        if($result){
            return $this->_fetch($result);
        }
    }

    protected function _escape($string)
    {
        return $this->_getConnection()->real_escape_string($string);
    }

    /**
     * @return \mysqli
     */
    protected function _getConnection()
    {
        return Client::getInstance()->getConnection();
    }

    /**
     * @param \mysqli_result $result
     */
    protected function _fetch($result)
    {

        $datas = array();
        while($data = $result->fetch_object())
        {
            $datas[] = $data;
        }

        $result->free();

        return $datas;
    }
}