<?php
namespace Library\MySQL;

class Client
{
    protected static $_instance = null;

    protected $_settings = array(
        'host' => null,
        'username' => null,
        'password' => null,
        'port' => null,
        'db' => null
    );

    protected $_db;

    /** @var $_connection \mysqli */
    protected $_connection;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if(self::$_instance == null){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function setSettings($settings)
    {
        if(empty($settings['host']))
            $settings['host'] = '127.0.0.1';

        if(empty($settings['username']))
            throw new \Exception('MySQL Client Error: Username is missing');

        if(empty($settings['password']))
            throw new \Exception('MySQL Client Error: Password is missing');

        if(empty($settings['port']))
            $settings['port'] = 3306;

        if(isset($settings['db'])){
            $this->setCurrentDB($settings['db']);
            unset($settings['db']);
        }

        $this->_settings = $settings;
    }

    public function connect()
    {
        if($this->isConnected()) return true;

        $this->_connection = new \mysqli();
        $this->_connection->init();

        $this->_connection->real_connect(
            $this->_settings['host'],
            $this->_settings['username'],
            $this->_settings['password'],
            $this->_db,
            $this->_settings['port']
        );

        if(mysqli_connect_errno())
            throw new \Exception('MySQL Client Error: Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
    }

    public function getConnection()
    {
        $this->connect();

        return $this->_connection;
    }

    public function setCurrentDB($db)
    {
        $this->_settings['db'] = $db;
        $this->_db = $db;
    }

    public function isConnected()
    {
        return ($this->_connection instanceof \mysqli);
    }

    public function __destruct()
    {
        if($this->isConnected())
            $this->_connection->close();
    }
}