<?php


namespace Library\Map;


/**
 * static class for representing the Grids on a map for clustering
 *
 *
 *
 * @package lib
 * @author  Richard Greenwood
 */
class Grids
{
    const POLAR_RADIUS = 6356.8; // in km
    const MERIDIONAL_CIRCUMFERENCE = 40007.86; // in km
    const EQUATORIAL_RADIUS = 6378.1; // in km

    const PENULTIMATE_LEVEL = 3,
        FINEST_LEVEL      = 4;

    //protected static $grid_sizes = array(5.625, 0.3515625, 0.02197265625);
    protected static $grid_sizes = array(14.4, 3.6, 0.72, 0.144, 0.072);


    /**
     * returns the grid size for the specified level
     *
     * @param int $level
     * @return float
     */
    public static function getSize($level)
    {
        return self::$grid_sizes[$level];
    }

    /**
     * returns the number of grid levels present
     *
     * @return int
     */
    public static function getNumberOfLevels()
    {
        return count(self::$grid_sizes);
    }

    /**
     * returns an array containing the 4 points of
     * the 10km grid box surrounding the given point
     * array(
     *   'topLeft'     => array('latitude' => *value*, 'longitude' => *value*),
     *   'topRight'    => array('latitude' => *value*, 'longitude' => *value*),
     *   'bottomLeft'  => array('latitude' => *value*, 'longitude' => *value*),
     *   'bottomRight' => array('latitude' => *value*, 'longitude' => *value*),
     * );
     *
     * @param float $lat the latitude of the point
     * @param float $long the longitude of the point
     * @return array
     */
    public static function get10kmGridBoxAroundPoint($lat, $long) {
        $gridBox = array(
            'topLeft' => array('latitude' => 0, 'longitude' => 0),
            'topRight' => array('latitude' => 0, 'longitude' => 0),
            'bottomLeft' => array('latitude' => 0, 'longitude' => 0),
            'bottomRight' => array('latitude' => 0, 'longitude' => 0),
        );

        $latDegreesPerKm = (360 / self::MERIDIONAL_CIRCUMFERENCE);

        $bottomLat = $lat - ($latDegreesPerKm * 5);
        $gridBox['bottomLeft']['latitude']  = $bottomLat;
        $gridBox['bottomRight']['latitude'] = $bottomLat;
        // get the approx radius for cross section of the earth at the southern latitude of the box
        $approxLatToAxisRadiusAtSouthEdge = self::getApproxRadiusFromLatitudeToEarthAxis($bottomLat);

        $topLat = $bottomLat + ($latDegreesPerKm * 5 * 2);
        $gridBox['topLeft']['latitude']  = $topLat;
        $gridBox['topRight']['latitude'] = $topLat;
        // get the approx radius for cross section of the earth at the northern latitude of the box
        $approxLatToAxisRadiusAtNorthEdge = self::getApproxRadiusFromLatitudeToEarthAxis($topLat);

        $longSouthDegreesPerKm = 360 / self::getCircumferenceFromRadius($approxLatToAxisRadiusAtSouthEdge);
        $longNorthDegreesPerKm = 360 / self::getCircumferenceFromRadius($approxLatToAxisRadiusAtNorthEdge);

        $gridBox['bottomRight']['longitude'] = $long - ($longSouthDegreesPerKm * 5);
        $gridBox['bottomLeft']['longitude']  = $gridBox['bottomRight']['longitude'] + ($longSouthDegreesPerKm * 5 * 2);
        $gridBox['topRight']['longitude'] = $long - ($longNorthDegreesPerKm * 5);
        $gridBox['topLeft']['longitude']  = $gridBox['topRight']['longitude'] + ($longNorthDegreesPerKm * 5 * 2);

        return $gridBox;
    }

    /**
     * returns the circumference of a circle for the given radius
     *
     * @param float $radius
     * @return float
     */
    public static function getCircumferenceFromRadius($radius)
    {
        return 2 * $radius * pi();
    }

    /**
     * returns an approximate value for the radius of the Earth
     * to the center point of the Earth in km from the given
     * latitude
     *
     * @param float $latitude
     * @return float
     */
    public static function getApproxRadiusFromLatitudeToEarthCenter($latitude)
    {
        // work out an approximation of how much shorter the radius is at the given latitude than at the
        // equatorial radius
        return self::EQUATORIAL_RADIUS - ($latitude * ((self::EQUATORIAL_RADIUS - self::POLAR_RADIUS) / 90));
    }

    /**
     * returns an approximate value for the radius of the
     * cross sectional circle of the Earth at the given
     * latitude in km
     *
     * @param float $latitude
     * @return float
     */
    public static function getApproxRadiusFromLatitudeToEarthAxis($latitude)
    {
        $h = self::getApproxRadiusFromLatitudeToEarthCenter($latitude);
        $oppositeDegree = 90 - $latitude;
        return $h * sin(deg2rad($oppositeDegree));
    }

    public static function getDistanceBetweenTwoPoints($lat1, $long1, $lat2, $long2) {
        // make an aproximate guess at the Earth's radius from the current location
        // THIS MIGHT BE BETTER TO USE A MEAN RADIUS VALUE
        $currentLatRadius =  self::getApproxRadiusFromLatitudeToEarthCenter($lat1);

        return $currentLatRadius * acos(sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($long2) - deg2rad($long1)));
    }

    /**
     * calculates the grid coordinate given a degree coordinate and the divisor
     *
     * @param float $degree
     * @param float $divisor
     * @return float
     */
    public static function calculateGridCoordinate($degree, $divisor)
    {
        return $degree >= 0 ? ceil($degree/$divisor) : floor($degree/$divisor);
    }
}