<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit5929e957c0e63e89352ff0cdfe14b6b6
{
    public static $prefixLengthsPsr4 = array (
        'M' => 
        array (
            'Model\\' => 6,
        ),
        'L' => 
        array (
            'Library\\' => 8,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Model\\' => 
        array (
            0 => __DIR__ . '/../..' . '/model',
        ),
        'Library\\' => 
        array (
            0 => __DIR__ . '/../..' . '/lib',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit5929e957c0e63e89352ff0cdfe14b6b6::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit5929e957c0e63e89352ff0cdfe14b6b6::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
