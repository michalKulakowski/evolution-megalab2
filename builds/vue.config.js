process.env.VUE_APP_BASE_URL = require('./config/app.config.json').appBaseUrl

module.exports = {
  // devServer: {
  //   open: process.platform === 'darwin',
  //   host: '192.168.1.15',
  //   port: 8080, // CHANGE YOUR PORT HERE!
  //   https: true,
  //   hotOnly: false
  // },
  configureWebpack: {
    devServer: {
      watchOptions: {
        ignored: ['/node_modules/', '/public/assets/'],
      }
    }
  },
  transpileDependencies: [
    'vuetify'
  ],
  publicPath: process.env.NODE_ENV === 'production'
    ? '/science/s317/evolution-megalab/public'
    : '/'
}
