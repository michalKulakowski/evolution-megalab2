<?php
require __DIR__ . '/../../app.php';

$model = new \Model\User();

try {
    echo json_encode(['status' => 200, 'body' => [
        'username' => $model->getUsername(),
        'fullname' => $model->getFullName(),
        'isLogged' => $model->checkIfLogged(),
        'isAdmin'  => $model->isAdmin(),
        'userId'   => $model->getUserId()
    ]]);
}catch (\Exception $ex){
    echo json_encode(['status' => 400, 'body' => ['msg' => $ex->getMessage()]]);
}
