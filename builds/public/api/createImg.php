<?php
require __DIR__ . '/../../app.php';

if (!file_exists(TMP_CLUSTER_DIR)) {
    mkdir(TMP_CLUSTER_DIR);
}
$count = array(
    'yellow' => 3,
    'brown' => 2,
    'pink' => 4,
    'other' => 0
);
$currentFilePath = TMP_CLUSTER_DIR.'/test2.gif';

\Library\Pins\Images::createPieChart($count, true, TMP_CLUSTER_DIR.'/test.gif', false);
\Library\Pins\Images::createPieChart($count, true, $currentFilePath, false);


$newFilePath = CLUSTER_DIR.'/testOK.gif';

$fileMoved = rename($currentFilePath, $newFilePath);

if($fileMoved){
    echo 'Success!';
}
