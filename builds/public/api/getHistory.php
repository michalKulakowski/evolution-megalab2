<?php
require __DIR__ . '/../../app.php';

$model = new \Model\Points();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $data = json_decode(file_get_contents("php://input"), true);
    $scope = @$data['scope'];
    if(empty($scope) || $scope == 'current'){
        $model = new \Model\Points();
    }elseif ($scope == 'historic'){
        $model = new \Model\Historic\Points();
    }else{
        echo json_encode(['status' => 400, 'body' => ['msg' => 'Incorrect scope!']]);
        die();
    }
    $level = $data['level'];
    $south = $data['south'];
    $west = $data['west'];
    $north = $data['north'];
    $east = $data['east'];
    echo json_encode(['status' => 200, 'body' => $model->getPoints($level, $south, $west, $north, $east)]);
}else{
    echo json_encode(['status' => 400, 'body' => ['msg' => 'Only POST request allowed!']]);
}

