<?php
require __DIR__ . '/../../app.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    try{
        $data = json_decode(file_get_contents("php://input"), true);
        $newRecordModel = new \Model\NewRecord();

        $newRecordModel->create($data);

        echo json_encode(['status' => 200, 'body' => ['msg' => 'Your data was saved.']]);
    }catch (\Exception $ex){
        echo json_encode(['status' => 400, 'body' => ['msg' => $ex->getMessage()]]);
    }
} else {
    echo json_encode(['status' => 400, 'body' => ['msg' => 'Only POST request allowed!']]);
}