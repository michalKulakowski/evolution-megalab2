<?php
//if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] === "off") {
//    $location = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
//    header('HTTP/1.1 301 Moved Permanently');
//    header('Location: ' . $location);
//    exit;
//}

header("HTTP/1.1 200 OK");
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

require __DIR__.'/vendor/autoload.php';

define('APP_DIR', __DIR__);
define('PINS_DIR', __DIR__.'/public/assets/pins');
define('CLUSTER_DIR', __DIR__.'/public/assets/cluster');
define('TMP_CLUSTER_DIR', __DIR__.'/public/assets/cluster/tmp');

define('HISTORIC_PINS_DIR', __DIR__.'/public/assets/historic/pins');
define('HISTORIC_CLUSTER_DIR', __DIR__.'/public/assets/historic/cluster');

error_reporting(E_ALL);
ini_set('display_errors', 1);

$MySQLSettings = require(APP_DIR.'/config/mysql.config.php');
\Library\MySQL\Client::getInstance()->setSettings($MySQLSettings);