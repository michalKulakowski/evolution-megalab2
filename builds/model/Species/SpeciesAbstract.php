<?php
namespace Model\Species;

use Library\MySQL\Model;

class SpeciesAbstract extends Model
{
    protected static $_table;

    protected static $con;

    public static function getInstanceId($paramsArray)
    {
        $fieldValuePairIds = self::_getFieldValuePairIds($paramsArray);

        $where = '';
        if(isset($fieldValuePairIds['colour'])){
            $where = 'colour_key_id = '.$fieldValuePairIds['colour'];
        }
        if(isset($fieldValuePairIds['bands'])){
            if(!empty($where)){
                $where .= ' AND ';
            }
            $where .= 'band_key_id = '.$fieldValuePairIds['bands'];
        }

        $query = 'SELECT id 
              FROM '.static::$_table;

        if(!empty($where)){
            $query .=' WHERE '.$where;
        }

        $res = self::_getDBInstance()->getOne($query);

        return $res->id;
    }

    public static function getTableName()
    {
        return static::$_table;
    }

    protected static function _getFieldValuePairIds($params)
    {
        $fieldValuePairIds = array();

        foreach($params as $f => $v)
        {
            $query = "SELECT fvp.`id` AS id, f.`name`
                FROM `field` f 
                JOIN `field_value_pair` fvp ON f.`id` = fvp.`field_id`
                WHERE f.`name` = '$f'
                AND fvp.`value` = '$v'";

            $value = self::_getDBInstance()->query($query);
            foreach ($value as $val){
                $fieldValuePairIds[$val->name] = $val->id;
            }
            unset($value);
        }

        return $fieldValuePairIds;
    }

    protected static function _getDBInstance()
    {
        if(!isset(self::$con)){
            self::$con = new self();
        }

        return self::$con;
    }
}