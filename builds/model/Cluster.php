<?php
namespace Model;


use Library\Map\Exception\EmptyClusterException;
use Library\Map\Grids;
use Library\MySQL\Model;
use Library\Pins\ColourCount;

class Cluster extends Model
{
    protected $_prefix = '';
    protected $_fields = [];
    public function getClustersForMap($level, $south, $west, $north, $east)
    {
        $res = $this->query('
        SELECT * 
        FROM '.$this->_prefix.'map_cluster
        WHERE `level` = '.$level.'
        AND `latitude` >= '. $south .'
        AND `latitude` <= '. $north .'
        AND `longitude` >= '. $west .'
        AND `longitude` <= '. $east
        );
        $result = array();
        foreach ($res as $val){
            $img = $this->getPathToCluster($val);
            if(!empty($img)){
                $result[] = array(
                    'id' => $val->id,
                    'type' => 'cluster',
                    'name' => '',
                    'latitude' => $val->latitude,
                    'longitude' => $val->longitude,
                    'img' => $img
                );
            }

        }
        return $result;
    }

    public function findCluster($level, $gridY, $gridX)
    {
        $gridBoxSize = Grids::getSize($level);

        $latitude = $gridY < 0 ? (($gridY * $gridBoxSize) + ($gridBoxSize/2)) : (($gridY * $gridBoxSize) - ($gridBoxSize/2));
        $longitude = $gridX < 0 ? (($gridX * $gridBoxSize) + ($gridBoxSize/2)) : (($gridX * $gridBoxSize) - ($gridBoxSize/2));

        // Check for an existing MapCluster Object
        return $this->getOne('SELECT * 
                FROM map_cluster 
                WHERE `latitude` = '.$latitude.' 
                AND `longitude` = '.$longitude.'
                AND `level` = '.$level);
    }

    public function createCluster($level, $gridY, $gridX)
    {
        $gridBoxSize = Grids::getSize($level);

        $latitude = $gridY < 0 ? (($gridY * $gridBoxSize) + ($gridBoxSize/2)) : (($gridY * $gridBoxSize) - ($gridBoxSize/2));
        $longitude = $gridX < 0 ? (($gridX * $gridBoxSize) + ($gridBoxSize/2)) : (($gridX * $gridBoxSize) - ($gridBoxSize/2));

        // Check for an existing MapCluster Object
        $cluster = $this->getOne('SELECT * 
                FROM map_cluster 
                WHERE `latitude` = '.$latitude.' 
                AND `longitude` = '.$longitude.'
                AND `level` = '.$level);
        if(empty($cluster)) // if a MapCluster doesn't exist
        {
            $imgName = $this->getImageName((object)[
                'level' => $level,
                'x' => $gridX,
                'y' => $gridY
            ], false);
            $clusterId = $this->insert('map_cluster', [
                'level' => $level,
                'latitude' => $latitude,
                'longitude' => $longitude,
                'y' => $gridY,
                'x' => $gridX,
                'image_name' => $imgName
            ]);

            return $clusterId;
        }
        return $cluster->id;
    }

    public function rebuildClusterImage($cluster, $filePath = null)
    {
        $clusterId = $cluster->id;

        $params = $this->countAllSpots($clusterId);
        $params = ColourCount::countAllColourSpotsPercentage($params);

        if(array_sum($params) == 0){
            throw new EmptyClusterException("Cluster with id: $clusterId is empty (no spots inside)");
        }

        if(empty($filePath))
            $filePath = CLUSTER_DIR.'/'.$this->getImageName($cluster);

        \Library\Pins\Images::createPieChart($params, true, $filePath, true);
    }

    public function countAllSpots($clusterId, $binomialName = null)
    {
        $eventTotal = array('yellow' => array(0 => 0, 1 => 0, 5 => 0), 'brown' => array(0 => 0, 1 => 0, 5 => 0), 'pink' => array(0 => 0, 1 => 0, 5 => 0));
        $where = '';
        if(isset($binomialName)){
            $where = 'WHERE binomial_name = "'.$this->_escape($binomialName).'"';
        }
        $species = $this->query('SELECT * FROM species '.$where);

        $coordinates = $this->_getCoordinatesForCluster($clusterId);
        if(empty($coordinates))
            throw new \Exception('Cannot to found cluster: '.$clusterId);

        foreach ($species as $spec){
            $table = $this->getSpeciesTableName($spec);
            if(empty($table))
                throw new \Exception('No table for species: '.$spec->binomial_name);

            $query = 'SELECT COUNT(*) as spot_count, s.*, c.*
            FROM spot s
            JOIN `'.$table.'` c ON c.`id` = s.`object_instance_id`
            WHERE
            s.`event_id` IN (
                SELECT e.id FROM  
                event e, location l 
                WHERE 
                e.location_id = l.id 
                AND l.latitude >= '.$coordinates['south'].' AND l.latitude < '.$coordinates['north'].' AND l.longitude >= '.$coordinates['west'].' AND l.longitude < '.$coordinates['east'].'
            )
            AND s.`species_id` = '.$spec->id.'
            GROUP BY s.`object_instance_id`';

            $res = $this->query($query);
            foreach ($res as $val){
                $colour = $this->_getFieldValue('colour', $val->colour_key_id);
                $band = $this->_getFieldValue('bands', $val->band_key_id);
                $eventTotal[$colour][(int)$band] += $val->spot_count;
            }
        }

        return $eventTotal;
    }

    public function getSpeciesTableName($spec)
    {
        if($spec->object_name){
            $method = new \ReflectionMethod("Model\\Species\\".$spec->object_name, 'getTableName');
            $id = $method->invoke(null);
            unset($method);
            return $id;
        }
        return null;
    }

    public function getEventIds($clusterId)
    {
        $coordinates = $this->_getCoordinatesForCluster($clusterId);
        $result = array();
        if(!empty($coordinates)){
            $query = "SELECT e.id FROM  ".$this->_prefix."event e, ".$this->_prefix."location l WHERE e.".$this->_prefix."location_id = l.id AND l.latitude >= ".$coordinates['south']." AND l.latitude < ".$coordinates['north']." AND l.longitude >= ".$coordinates['west']." AND l.longitude < ".$coordinates['east'];

            $eventsRS = $this->query($query);
            foreach ($eventsRS as $val){
                $result[] = $val->id;
            }
            unset($eventsRS);
        }

        return $result;
    }

    protected function _getCoordinatesForCluster($clusterId)
    {
        $cluster = $this->getOne('SELECT * FROM '.$this->_prefix.'map_cluster WHERE id = '.$clusterId);
        $result = [];
        if(!empty($cluster)){
            $mapGridSize = Grids::getSize($cluster->level);
            $south = $cluster->y * $mapGridSize;
            $south = $cluster->y > 0 ? $south - $mapGridSize : $south;
            $north = $south + $mapGridSize;
            $west = $cluster->x * $mapGridSize;
            $west = $cluster->x > 0 ? $west - $mapGridSize : $west;
            $east = $west + $mapGridSize;

            $result['south'] = $south;
            $result['north'] = $north;
            $result['west'] = $west;
            $result['east'] = $east;
        }

        return $result;
    }

    public function getPathToCluster($cluster)
    {
        $result = array();
        $fileName = $this->getImageName($cluster);
        $url = CLUSTER_DIR . '/'.$fileName;
        if(file_exists($url))
            $result['url'] = '/assets/cluster/'.$fileName;

        $fileName = $this->getImageName($cluster, true);
        $url = CLUSTER_DIR . '/'.$fileName;
        if(file_exists($url))
            $result['selectedUrl'] = '/assets/cluster/'.$fileName;

        return $result;
    }

    protected function _getFieldValue($fieldType, $fieldId)
    {
        if(empty($this->_fields)){
            $res = $this->query('SELECT fv.`id` as field_id, f.`name` as field_type, fv.`value`
                FROM `field` f
                JOIN `field_value_pair` fv ON f.id = fv.`field_id`');
            foreach ($res as $field){
                if(empty($this->_fields[$field->field_type]))
                    $this->_fields[$field->field_type] = [];

                $this->_fields[$field->field_type][$field->field_id] = $field->value;
            }
        }
        if(!isset($this->_fields[$fieldType][$fieldId]))
            throw new \Exception("Field with id: $fieldId not supported");

        return $this->_fields[$fieldType][$fieldId];
    }

    public function getImageName($cluster, $selected = false)
    {
        $y = str_replace('-', 'm', $cluster->y);
        $x = str_replace('-', 'm', $cluster->x);
        if($selected){
            return 'pin_' . $cluster->level . '_' . $y . '_' . $x . '_selected.gif';
        }else{
            return 'pin_' . $cluster->level . '_' . $y . '_' . $x . '.gif';
        }

    }
}