<?php


namespace Model;


use Library\MySQL\Model;

class Event extends Model
{
    protected $_spotsModel = null;
    protected $_prefix;

    public function getEvents($south, $west, $north, $east)
    {
        $result = array();
        $res = $this->query('SELECT l.*, e.id as event_id, e.created_by
                From `'.$this->_prefix.'location` l
                JOIN `'.$this->_prefix.'event` e ON e.`'.$this->_prefix.'location_id` = l.id 
                WHERE
                `latitude` >= '. $south .'
                AND `latitude` <= '. $north .'
                AND `longitude` >= '. $west .'
                AND `longitude` <= '. $east);
        $userModel = new User();
        $userID = $userModel->getUserId();
        foreach ($res as $r){
            $img = $this->getPathToPin($r->event_id, ((int)$userID == (int)$r->created_by));
            if(!empty($img)){
                $result[] = array(
                    'id' => $r->event_id,
                    'type' => 'pin',
                    'name' => $r->name,
                    'latitude' => $r->latitude,
                    'longitude' => $r->longitude,
                    'img' => $img
                );
            }
        }

        return $result;
    }

    /**
     * @param int $eventId
     * @param bool $userPin
     * @return array
     */
    public function getPathToPin($eventId, $userPin)
    {
        $result = array();

        $userFileName = 'pin'.$eventId.'_User.gif';
        $fileName = 'pin'.$eventId.'.gif';
        $url = PINS_DIR . '/'.$fileName;
        if($userPin && file_exists(PINS_DIR . '/'.$userFileName))
            $result['url'] = '/assets/pins/'.$userFileName;
        elseif (file_exists($url))
            $result['url'] = '/assets/pins/'.$fileName;

        $userFileName = 'pin'.$eventId.'_selectedUser.gif';
        $fileName = 'pin'.$eventId.'_selected.gif';
        $url = PINS_DIR . '/'.$fileName;
        if($userPin && file_exists(PINS_DIR . '/'.$userFileName))
            $result['selectedUrl'] = '/assets/pins/'.$userFileName;
        elseif (file_exists($url))
            $result['selectedUrl'] = '/assets/pins/'.$fileName;

        return $result;
    }

    public function countAllColourSpots($eventId, $binomialName = null)
    {
        $res = $this->countAllSpots($eventId, $binomialName);
        $eventTotal = array('yellow' => 0, 'brown' => 0, 'pink' => 0);
        foreach ($res as $colour => $data){
            $eventTotal[$colour] = array_sum($data);
        }

        return $eventTotal;
    }

    public function countAllSpots($eventId, $binomialName = null)
    {
        $species = [];
        $eventTotal = array('yellow' => array(0 => 0, 1 => 0, 5 => 0), 'brown' => array(0 => 0, 1 => 0, 5 => 0), 'pink' => array(0 => 0, 1 => 0, 5 => 0));
        $where = '';
        if(isset($binomialName)){
            $where = 'WHERE binomial_name = "'.$this->_escape($binomialName).'"';
        }
        $sptmgr = $this->_getSpotsModel();
        $sptmgr->setEventId($eventId);
        $species = $this->query('SELECT * FROM species '.$where);

        foreach ($species as $spec){
            $sptmgr->setSpecies($spec);

            foreach (array('yellow','pink','brown') as $colour){
                foreach (array(0,1,5) as $band){
                    $eventTotal[$colour][$band] += $sptmgr->countSpots(array (
                        'colour' => $colour,
                        'bands' => $band
                    ));
                }
            }
        }

        return $eventTotal;
    }

    public function countAllColourSpotsPercentage($eventId, $binomialName = null)
    {
        $eventTotal = $this->countAllColourSpots($eventId, $binomialName);

        $total = array_sum($eventTotal);
        if($total > 0){
            return array(
                'yellow' => ($eventTotal['yellow'] / $total) * 100,
                'brown' => ($eventTotal['brown'] / $total) * 100,
                'pink' => ($eventTotal['pink'] / $total) * 100,
                'other' => 0
            );
        }else{
            return array(
                'yellow' => 0,
                'brown' => 0,
                'pink' => 0,
                'other' => 0
            );
        }
    }

    /**
     * @return Spots
     */
    protected function _getSpotsModel()
    {
        if(empty($this->_spotsModel)){
            $this->_spotsModel = new Spots();
        }
        return $this->_spotsModel;
    }
}