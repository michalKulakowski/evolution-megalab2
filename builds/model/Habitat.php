<?php
namespace Model;


use Library\MySQL\Model;

class Habitat extends Model
{
    public function getHabitatTypes()
    {
        return $this->query('SELECT h.*, hi.`name` FROM habitat_i18n hi
            JOIN habitat h ON h.`habitat_number` = hi.`id`
            WHERE culture = "en"');
    }
}