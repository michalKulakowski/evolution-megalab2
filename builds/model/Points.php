<?php
namespace Model;

use Library\Map\Grids;
use Library\MySQL\Model;

class Points extends Model
{
    protected $_evolutionModel = null;
    protected $_userId = null;
    protected $_scope = 'current';
    protected $_colours = [
        'yellow' => [
            0 => '#FFECB3',
            1 => '#FFD54F',
            5 => '#FFC107'
        ],
        'pink' => [
            0 => '#F8BBD0',
            1 => '#F06292',
            5 => '#E91E63'
        ],
        'brown' => [
            0 => '#D7CCC8',
            1 => '#A1887F',
            5 => '#795548'
        ],
    ];

    public function getPoints($level, $south, $west, $north, $east)
    {
        if($level <= Grids::FINEST_LEVEL){
            $model = new Cluster();

            return $model->getClustersForMap($level, $south, $west, $north, $east);
        }else{
            $model = new Event();

            return $model->getEvents($south, $west, $north, $east);
        }
    }

    public function getPointInfo($eventId)
    {
        $result = array();
        $result['record'] = $this->getRecordData($eventId);
        $result['nemoralis'] = $this->_getDataByBinomialName($eventId, 'Cepaea nemoralis');
        $result['hortensis'] = $this->_getDataByBinomialName($eventId, 'Cepaea hortensis');

        return $result;
    }

    public function changeFavoriteStatus($eventId, $scope)
    {
        if(empty($eventId))
            throw new \Exception('Event ID is required!');
        if(empty($scope) || !in_array($scope,['current','historic']))
            throw new \Exception('Scope is required!');

        if(!$this->_isFavorite($eventId, $scope)){
            $this->insert('user_favorite', [
                'user_id' => $this->_getUserId(),
                'event_id' => $eventId,
                'scope' => $scope,
                'created_at' => date('Y-m-d H:i:s')
            ]);

            return 'Point has been mark as a favorite.';
        }else{
            $this->delete('DELETE FROM user_favorite WHERE user_id = '.(int)$this->_getUserId().'
            AND event_id = '.(int)$this->_escape($eventId).'
            AND `scope` = "'.(string)$this->_escape($scope).'"');
            return 'Point has been removed from favorite.';
        }
    }

    protected function _isFavorite($eventId, $scope = null)
    {
        if(empty($scope))
            $scope = $this->_scope;

        $userId = $this->_getUserId();

        $res = $this->getOne('SELECT * 
            FROM user_favorite
            WHERE user_id = '.$userId.'
            AND event_id = '.(int)$this->_escape($eventId).'
            AND `scope` = "'.(string)$this->_escape($scope).'"'
        );

        return !empty($res);
    }

    protected function _getUserId()
    {
        if(empty($this->_userId)){
            $userModel = new User();
            $this->_userId = $userModel->getUserId();
        }

        return (int)$this->_userId;
    }

    public function getRecordData($eventId)
    {
        $res = $this->getOne('SELECT e.*, YEAR(e.date) as year_added, l.`latitude`, l.`longitude`, hn.`name` as habitat_name, u.`username`
            FROM `event` e 
            JOIN `location` l ON e.`location_id` = l.id
            JOIN `sf_guard_user` u ON u.id = e.`created_by`
            LEFT JOIN `habitat` h ON h.`id` = l.`habitat_id`
            LEFT JOIN `habitat_i18n` hn ON h.`habitat_number` = hn.`id` AND hn.`culture` = "en"
            WHERE
            e.`id` = '.(int)$this->_escape($eventId));
        if(!empty($res))
            return array(
            'eventId' => $eventId,
            'name' => $res->name,
            'is_favorite' => $this->_isFavorite($eventId),
            'recored_by'=> array(
                'user' => $res->username,
                'date' => $res->date,
                'userId' => $res->created_by
            ),
            'comment' => $res->comment,
            'year' => $res->year_added,
            'habitat_type' => $res->habitat_name,
            'lat' => $res->latitude,
            'lng' => $res->longitude
            );
        else
            return null;
    }

    protected function _getDataByBinomialName($eventId, $binomialName)
    {
        $model = new Event();

        $res = $model->countAllSpots($eventId, $binomialName);

        $total = 0;
        $result = array(
            'name' => $this->_getTitle($binomialName),
            'total' => 0,
            'info' => array()
        );
        foreach ($res as $colour => $val){
            $result['total'] += array_sum($val);
            foreach ($val as $band => $num){
                $result['info'][] = array(
                    'label' => $this->_getLabelForSpot($colour, $band),
                    'value' => $num,
                    'color' => $this->_colours[$colour][(int)$band]
                );
            }
        }

        if($result['total'] > 0){
//            $result['evolution'] = $this->_getEvolutionModel()->getData($eventId, $binomialName);
            $result['evolution'] = null;
            return $result;
        } else {
            return null;
        }
    }

    /**
     * @return Evolution
     */
    protected function _getEvolutionModel()
    {
        if(empty($this->_evolutionModel)){
            $this->_evolutionModel = new Evolution();
        }
        return $this->_evolutionModel;
    }

    protected function _getTitle($binomialName)
    {
        switch ($binomialName){
            case 'Cepaea nemoralis': return 'Brown Lipped Snails - Cepaea nemoralis'; break;
            case 'Cepaea hortensis': return 'White Lipped Snails - Cepaea hortensis'; break;
        }
        return '';
    }

    protected function _getLabelForSpot($colour, $band)
    {
        $colour = ucfirst(strtolower($colour));
        switch ($band){
            case 0: return $colour .' with 0 bands'; break;
            case 1: return $colour .' with 1 band'; break;
            case 5: return $colour .' with many bands'; break;
        }
    }
}