<?php


namespace Model\Historic;


class Event extends \Model\Event
{
    protected $_prefix = 'historic_';

    protected function _getSpotsModel()
    {
        if(empty($this->_spotsModel)){
            $this->_spotsModel = new Spots();
        }
        return $this->_spotsModel;
    }

    public function getEvents($south, $west, $north, $east)
    {
        $result = array();
        $res = $this->query('SELECT l.*, e.id as event_id
                From `'.$this->_prefix.'location` l
                JOIN `'.$this->_prefix.'event` e ON e.`'.$this->_prefix.'location_id` = l.id 
                WHERE
                `latitude` >= '. $south .'
                AND `latitude` <= '. $north .'
                AND `longitude` >= '. $west .'
                AND `longitude` <= '. $east);

        foreach ($res as $r){
            $img = $this->getPathToPin($r->event_id, false);
            if(!empty($img)){
                $result[] = array(
                    'id' => $r->event_id,
                    'name' => $r->name,
                    'type' => 'pin',
                    'latitude' => $r->latitude,
                    'longitude' => $r->longitude,
                    'img' => $img
                );
            }
        }

        return $result;
    }

    public function getPathToPin($eventId, $userPin)
    {
        $result = array();
        $fileName = 'pin'.$eventId.'.gif';
        $url = PINS_DIR . '/'.$fileName;
        if(file_exists($url))
            $result['url'] = '/assets/historic/pins/'.$fileName;

        $fileName = 'pin'.$eventId.'_selected.gif';
        $url = PINS_DIR . '/'.$fileName;
        if(file_exists($url))
            $result['selectedUrl'] = '/assets/historic/pins/'.$fileName;

        return $result;
    }
}