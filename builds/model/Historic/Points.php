<?php
namespace Model\Historic;

use Library\Map\Grids;

class Points extends \Model\Points
{
    protected $_scope = 'historic';

    public function getPoints($level, $south, $west, $north, $east)
    {
        if($level <= Grids::FINEST_LEVEL){
            $model = new Cluster();

            return $model->getClustersForMap($level, $south, $west, $north, $east);
        }else{
            $model = new Event();

            return $model->getEvents($south, $west, $north, $east);
        }
    }

    public function getRecordData($eventId)
    {
        $res = $this->getOne('SELECT e.*, YEAR(e.date) as year_added, l.`latitude`, l.`longitude`, hn.`name` as habitat_name
            FROM `historic_event` e 
            JOIN `historic_location` l ON e.`historic_location_id` = l.id
            LEFT JOIN `habitat` h ON h.`id` = l.`habitat_id`
            LEFT JOIN `habitat_i18n` hn ON h.`habitat_number` = hn.`id` AND hn.`culture` = "en"
            WHERE
            e.`id` = '.(int)$this->_escape($eventId));
        if(!empty($res))
            return array(
                'eventId' => $eventId,
                'name' => $res->name,
                'recored_by'=> array(
                    'user' => null,
                    'date' => $res->date
                ),
                'comment' => $res->comment,
                'is_favorite' => $this->_isFavorite($eventId),
                'year' => $res->year_added,
                'habitat_type' => $res->habitat_name,
                'lat' => $res->latitude,
                'lng' => $res->longitude
            );
        else
            return null;
    }

    protected function _getDataByBinomialName($eventId, $binomialName)
    {
        $model = new Event();

        $res = $model->countAllSpots($eventId, $binomialName);

        $result = array(
            'name' => $this->_getTitle($binomialName),
            'total' => 0,
            'info' => array()
        );
        foreach ($res as $colour => $val){
            $result['total'] += array_sum($val);
            foreach ($val as $band => $num){
                $result['info'][] = array(
                    'label' => $this->_getLabelForSpot($colour, $band),
                    'value' => $num,
                    'color' => $this->_colours[$colour][(int)$band]
                );
            }
        }

        if($result['total'] > 0){
            $result['evolution'] = null;
            return $result;
        } else {
            return null;
        }
    }
}