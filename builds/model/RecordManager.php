<?php


namespace Model;

use Library\MySQL\Model;

class RecordManager extends Model
{
    protected $_userId = null;

    /** @var Cluster */
    protected $_clusterModel;

    protected $_createdFiles = [
        'event' => [],
        'cluster' => []
    ];

    public function __construct()
    {
        $this->_clusterModel = new Cluster();
        if (!file_exists(TMP_CLUSTER_DIR)) {
            mkdir(TMP_CLUSTER_DIR);
        }

        $userModel = new User();
        $this->_userId = $userModel->getUserId();
    }

    protected function _removeFiles()
    {
        foreach ($this->_createdFiles['cluster'] as $file){
            if(file_exists(TMP_CLUSTER_DIR.'/'.$file['tmp']['file']))
                unlink(TMP_CLUSTER_DIR.'/'.$file['tmp']['file']);

            if(file_exists(TMP_CLUSTER_DIR.'/'.$file['tmp']['selected']))
                unlink(TMP_CLUSTER_DIR.'/'.$file['tmp']['selected']);
        }
    }

    protected function _getClusterPathInTmp($cluster)
    {
        $fileName = $this->_clusterModel->getImageName($cluster);
        $selectedFileName = $this->_clusterModel->getImageName($cluster, true);
        $tmpFileName = $this->_userId.'_'.$fileName;
        $tmpSelectedFileName = $this->_userId.'_'.$selectedFileName;
        $path = TMP_CLUSTER_DIR . '/'.$tmpFileName;

        $this->_createdFiles['cluster'][] = [
            'tmp' => [
                'file' => $tmpFileName,
                'selected' => $tmpSelectedFileName
            ],
            'new' => [
                'file' => $fileName,
                'selected' => $selectedFileName
            ]
        ];

        return $path;
    }

    protected function _moveClusterFiles()
    {
        foreach ($this->_createdFiles['cluster'] as $file){
            if(file_exists(TMP_CLUSTER_DIR.'/'.$file['tmp']['file'])){
                rename(TMP_CLUSTER_DIR.'/'.$file['tmp']['file'], CLUSTER_DIR.'/'.$file['new']['file']);
            }
            if(file_exists(TMP_CLUSTER_DIR.'/'.$file['tmp']['selected'])){
                rename(TMP_CLUSTER_DIR.'/'.$file['tmp']['selected'], CLUSTER_DIR.'/'.$file['new']['selected']);
            }
        }
    }
}