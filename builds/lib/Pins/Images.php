<?php


namespace Library\Pins;


class Images
{
    /**
     * draws a pie chart image as a pin for a map
     *
     * @param array $paramArray
     * @param boolean $selected
     * @param boolean $toFile
     * @param string $filePath
     */
    public static function createPieChartPin($paramArray, $selected = false, $toFile = false, $filePath = null)
    {
        self::_createUserPieChartPin($paramArray, $selected, $toFile, $filePath);
        $s = 8;
        $realWidth = 21;
        $realHeight = 35;
        $srcWidth = $realWidth * $s;
        $srcHeight = $realHeight * $s;

        // create the larger source image
        $image = imagecreatetruecolor($srcWidth, $srcHeight);
        // create the real/final image
        $destImage = imagecreatetruecolor($realWidth, $realHeight);
        imagesetthickness($image, 8);
        imageantialias($image, true);

        // allocate some colours
        $white = imagecolorallocate($image, 255, 255, 255);
        $black = imagecolorallocate($image, 0, 0, 0);
        $charcoal = imagecolorallocate($image, 40, 40, 40);
        $orange = imagecolorallocate($image, 0xef, 0x64, 0x21);
        $red = imagecolorallocate($image, 0xcc, 0x00, 0x00);

        $grey = imagecolorallocate($image, 207, 207, 207);
        $yellow = imagecolorallocate($image, 227, 209, 57);
        $pink = imagecolorallocate($image, 203, 154, 169);
        $brown = imagecolorallocate($image, 130, 84, 54);

        imagecolortransparent($image, $black);
        imagecolortransparent($destImage, $black);

        if($selected)
        {
            // create the larger source image
            $selectedImage = imagecreatetruecolor($srcWidth, $srcHeight);
            // create the real/final image
            $selectedDestImage = imagecreatetruecolor($realWidth, $realHeight);
            imagesetthickness($selectedImage, 8);
            imageantialias($selectedImage, true);

            // allocate some colours
            $whiteSelected = imagecolorallocate($selectedImage, 255, 255, 255);
            $blackSelected = imagecolorallocate($selectedImage, 0, 0, 0);
            $charcoalSelected = imagecolorallocate($selectedImage, 40, 40, 40);
            $orangeSelected = imagecolorallocate($selectedImage, 0xef, 0x64, 0x21);
            $redSelected = imagecolorallocate($selectedImage, 0xcc, 0x00, 0x00);

            $greySelected = imagecolorallocate($selectedImage, 207, 207, 207);
            $yellowSelected = imagecolorallocate($selectedImage, 227, 209, 57);
            $pinkSelected = imagecolorallocate($selectedImage, 203, 154, 169);
            $brownSelected = imagecolorallocate($selectedImage, 130, 84, 54);

            imagecolortransparent($selectedImage, $blackSelected);
            imagecolortransparent($selectedDestImage, $blackSelected);
        }

        //$edge = $selected ? $red : $charcoal;

        // create the pin locator
        $values = array (
            16 * $s,
            15 * $s, // Point 1 (x, y)
            11 * $s,
            35 * $s, // Point 2 (x, y)
            6 * $s,
            15 * $s, // Point 3 (x, y)


        );
        imagefilledpolygon($image, $values, 3, $orange);
        if($selected)
        {
            imagefilledpolygon($selectedImage, $values, 3, $orangeSelected);
        }
        $values = array (
            16 * $s,
            15 * $s, // Point 1 (x, y)
            11 * $s,
            35 * $s, // Point 2 (x, y)
            6 * $s,
            15 * $s, // Point 3 (x, y)


        );

        imagepolygon($image, $values, 3, $charcoal);
        if($selected)
        {
            imagepolygon($selectedImage, $values, 3, $redSelected);
        }

        // create the pie chart
        $notSnails = $paramArray['other'];
        unset ($paramArray['other']);
        $itemCount = count($paramArray);
        //asort($paramArray);
        $total = array_sum($paramArray);
        if ($total > 0)
        {
            $percentMulti = 100 / $total;
            $degMulti = 360 / 100;
            $degBegin = 0;
            $i = 0;
            $runningTotal = 0;
            foreach ($paramArray as $colourVariable => $amount)
            {
                $runningTotal += $amount;
                if($runningTotal == $total)
                {
                    imagefilledarc($image, 11 * $s, 11 * $s, 19 * $s, 19 * $s, $degBegin, 360, ${ $colourVariable }, IMG_ARC_PIE);
                    if($selected)
                    {
                        imagefilledarc($selectedImage, 11 * $s, 11 * $s, 19 * $s, 19 * $s, $degBegin, 360, ${ $colourVariable.'Selected' }, IMG_ARC_PIE);
                    }
                    break;
                }
                elseif($amount > 0)
                {
                    $degAmount = floor(($amount * $percentMulti * $degMulti) + $degBegin);
                    imagefilledarc($image, 11 * $s, 11 * $s, 19 * $s, 19 * $s, $degBegin, $degAmount, ${ $colourVariable }, IMG_ARC_PIE);
                    if($selected)
                    {
                        imagefilledarc($selectedImage, 11 * $s, 11 * $s, 19 * $s, 19 * $s, $degBegin, $degAmount, ${ $colourVariable.'Selected' }, IMG_ARC_PIE);
                    }
                    $degBegin = $degAmount;
                }
            }
            unset($paramArray);
        }
        else
        {
            if ($notSnails > 0)
            {
                // only other species at this location
                imagefilledarc($image, 11 * $s, 11 * $s, 19 * $s, 19 * $s, 0, 360, $grey, IMG_ARC_PIE);
                if($selected)
                {
                    imagefilledarc($selectedImage, 11 * $s, 11 * $s, 19 * $s, 19 * $s, 0, 360, $greySelected, IMG_ARC_PIE);
                }
            } else
            {
                // no records at all for this location!!
                imagefilledarc($image, 11 * $s, 11 * $s, 19 * $s, 19 * $s, 0, 360, $white, IMG_ARC_PIE);
                if($selected)
                {
                    imagefilledarc($selectedImage, 11 * $s, 11 * $s, 19 * $s, 19 * $s, 0, 360, $whiteSelected, IMG_ARC_PIE);
                }
            }
        }
        imagearc($image, 11 * $s, 11 * $s, 19 * $s, 19 * $s, 0, 359, $charcoal);
        imagearc($image, 11 * $s, 11 * $s, 19 * $s, 19 * $s, 359, 360, $charcoal);
        if ($selected)
        {
            imagearc($selectedImage, 11 * $s, 11 * $s, 19 * $s, 19 * $s, 0, 359, $redSelected);
            imagearc($selectedImage, 11 * $s, 11 * $s, 19 * $s, 19 * $s, 359, 360, $redSelected);
            imagegammacorrect($selectedImage, 1.0, 0.5);
        }

        // now the picture is finished, do the shrink...
        imagecopyresampled($destImage, $image, 0, 0, 0, 0, $realWidth, $realHeight, $srcWidth, $srcHeight);
        if ($selected)
        {
            imagecopyresampled($selectedDestImage, $selectedImage, 0, 0, 0, 0, $realWidth, $realHeight, $srcWidth, $srcHeight);
        }

        // flush image
        if ($toFile)
        {
            //$imageName = SF_ROOT_DIR.DIRECTORY_SEPARATOR.'web'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'pins'.DIRECTORY_SEPARATOR.$fileName . '.gif';
            imagegif($destImage, $filePath);
            if ($selected)
            {
                $imageNameArray = explode('.', $filePath);
                $imageNameArray[count($imageNameArray) - 2] = $imageNameArray[count($imageNameArray) - 2] . '_selected';
                $filePath = implode('.', $imageNameArray);
                imagegif($selectedDestImage, $filePath);
            }
        } else
        {
            imagegif($destImage);
            // Creating the selected version of the image at the same time means that the images must always be written to file.
            // So this whole else statment can't really be used!!
            /*if ($selected)
            {
              imagegif($selectedDestImage);
            }*/
        }
        imagedestroy($image);
        imagedestroy($destImage);
        unset($image, $destImage);
        if ($selected)
        {
            imagedestroy($selectedImage);
            imagedestroy($selectedDestImage);
            unset($selectedImage, $selectedDestImage);
        }
        unset($paramArray, $s, $realWidth, $realHeight, $srcWidth, $srcHeight, $white, $black, $charcoal, $orange, $red, $grey, $yellow, $pink, $brown, $edge);
        unset($itemCount, $total, $percentMulti, $degMulti, $degBegin);
    }

    /**
     * draws a pie chart image as a pin for a map
     *
     * @param array $paramArray
     * @param boolean $selected
     * @param boolean $toFile
     * @param string $filePath
     */
    public static function _createUserPieChartPin($paramArray, $selected = false, $toFile = false, $filePath = null)
    {
        $s = 8;
        $realWidth = 21;
        $realHeight = 35;
        $srcWidth = $realWidth * $s;
        $srcHeight = $realHeight * $s;

        // create the larger source image
        $image = imagecreatetruecolor($srcWidth, $srcHeight);
        // create the real/final image
        $destImage = imagecreatetruecolor($realWidth, $realHeight);
        imagesetthickness($image, 8);
        imageantialias($image, true);

        // allocate some colours
        $white = imagecolorallocate($image, 255, 255, 255);
        $black = imagecolorallocate($image, 0, 0, 0);
        $charcoal = imagecolorallocate($image, 40, 40, 40);
        $orange = imagecolorallocate($image, 0, 200, 83);
        $red = imagecolorallocate($image, 0xcc, 0x00, 0x00);

        $grey = imagecolorallocate($image, 207, 207, 207);
        $yellow = imagecolorallocate($image, 227, 209, 57);
        $pink = imagecolorallocate($image, 203, 154, 169);
        $brown = imagecolorallocate($image, 130, 84, 54);

        imagecolortransparent($image, $black);
        imagecolortransparent($destImage, $black);

        if($selected)
        {
            // create the larger source image
            $selectedImage = imagecreatetruecolor($srcWidth, $srcHeight);
            // create the real/final image
            $selectedDestImage = imagecreatetruecolor($realWidth, $realHeight);
            imagesetthickness($selectedImage, 8);
            imageantialias($selectedImage, true);

            // allocate some colours
            $whiteSelected = imagecolorallocate($selectedImage, 255, 255, 255);
            $blackSelected = imagecolorallocate($selectedImage, 0, 0, 0);
            $charcoalSelected = imagecolorallocate($selectedImage, 40, 40, 40);
            $orangeSelected = imagecolorallocate($image, 0, 200, 83);
            $redSelected = imagecolorallocate($selectedImage, 0xcc, 0x00, 0x00);

            $greySelected = imagecolorallocate($selectedImage, 207, 207, 207);
            $yellowSelected = imagecolorallocate($selectedImage, 227, 209, 57);
            $pinkSelected = imagecolorallocate($selectedImage, 203, 154, 169);
            $brownSelected = imagecolorallocate($selectedImage, 130, 84, 54);

            imagecolortransparent($selectedImage, $blackSelected);
            imagecolortransparent($selectedDestImage, $blackSelected);
        }

        //$edge = $selected ? $red : $charcoal;

        // create the pin locator
        $values = array (
            16 * $s,
            15 * $s, // Point 1 (x, y)
            11 * $s,
            35 * $s, // Point 2 (x, y)
            6 * $s,
            15 * $s, // Point 3 (x, y)


        );
        imagefilledpolygon($image, $values, 3, $orange);
        if($selected)
        {
            imagefilledpolygon($selectedImage, $values, 3, $orangeSelected);
        }
        $values = array (
            16 * $s,
            15 * $s, // Point 1 (x, y)
            11 * $s,
            35 * $s, // Point 2 (x, y)
            6 * $s,
            15 * $s, // Point 3 (x, y)


        );

        imagepolygon($image, $values, 3, $charcoal);
        if($selected)
        {
            imagepolygon($selectedImage, $values, 3, $redSelected);
        }

        // create the pie chart
        $notSnails = $paramArray['other'];
        unset ($paramArray['other']);
        $itemCount = count($paramArray);
        //asort($paramArray);
        $total = array_sum($paramArray);
        if ($total > 0)
        {
            $percentMulti = 100 / $total;
            $degMulti = 360 / 100;
            $degBegin = 0;
            $i = 0;
            $runningTotal = 0;
            foreach ($paramArray as $colourVariable => $amount)
            {
                $runningTotal += $amount;
                if($runningTotal == $total)
                {
                    imagefilledarc($image, 11 * $s, 11 * $s, 19 * $s, 19 * $s, $degBegin, 360, ${ $colourVariable }, IMG_ARC_PIE);
                    if($selected)
                    {
                        imagefilledarc($selectedImage, 11 * $s, 11 * $s, 19 * $s, 19 * $s, $degBegin, 360, ${ $colourVariable.'Selected' }, IMG_ARC_PIE);
                    }
                    break;
                }
                elseif($amount > 0)
                {
                    $degAmount = floor(($amount * $percentMulti * $degMulti) + $degBegin);
                    imagefilledarc($image, 11 * $s, 11 * $s, 19 * $s, 19 * $s, $degBegin, $degAmount, ${ $colourVariable }, IMG_ARC_PIE);
                    if($selected)
                    {
                        imagefilledarc($selectedImage, 11 * $s, 11 * $s, 19 * $s, 19 * $s, $degBegin, $degAmount, ${ $colourVariable.'Selected' }, IMG_ARC_PIE);
                    }
                    $degBegin = $degAmount;
                }
            }
            unset($paramArray);
        }
        else
        {
            if ($notSnails > 0)
            {
                // only other species at this location
                imagefilledarc($image, 11 * $s, 11 * $s, 19 * $s, 19 * $s, 0, 360, $grey, IMG_ARC_PIE);
                if($selected)
                {
                    imagefilledarc($selectedImage, 11 * $s, 11 * $s, 19 * $s, 19 * $s, 0, 360, $greySelected, IMG_ARC_PIE);
                }
            } else
            {
                // no records at all for this location!!
                imagefilledarc($image, 11 * $s, 11 * $s, 19 * $s, 19 * $s, 0, 360, $white, IMG_ARC_PIE);
                if($selected)
                {
                    imagefilledarc($selectedImage, 11 * $s, 11 * $s, 19 * $s, 19 * $s, 0, 360, $whiteSelected, IMG_ARC_PIE);
                }
            }
        }
        imagearc($image, 11 * $s, 11 * $s, 19 * $s, 19 * $s, 0, 359, $charcoal);
        imagearc($image, 11 * $s, 11 * $s, 19 * $s, 19 * $s, 359, 360, $charcoal);
        if ($selected)
        {
            imagearc($selectedImage, 11 * $s, 11 * $s, 19 * $s, 19 * $s, 0, 359, $redSelected);
            imagearc($selectedImage, 11 * $s, 11 * $s, 19 * $s, 19 * $s, 359, 360, $redSelected);
            imagegammacorrect($selectedImage, 1.0, 0.5);
        }

        // now the picture is finished, do the shrink...
        imagecopyresampled($destImage, $image, 0, 0, 0, 0, $realWidth, $realHeight, $srcWidth, $srcHeight);
        if ($selected)
        {
            imagecopyresampled($selectedDestImage, $selectedImage, 0, 0, 0, 0, $realWidth, $realHeight, $srcWidth, $srcHeight);
        }

        // flush image
        if ($toFile)
        {
            //$imageName = SF_ROOT_DIR.DIRECTORY_SEPARATOR.'web'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'pins'.DIRECTORY_SEPARATOR.$fileName . '.gif';
            $imageNameArray = explode('.', $filePath);
            $name = $imageNameArray[count($imageNameArray) - 2];
            $imageNameArray[count($imageNameArray) - 2] = $name . '_User';
            $filePath = implode('.', $imageNameArray);

            imagegif($destImage, $filePath);
            if ($selected)
            {
                $imageNameArray[count($imageNameArray) - 2] = $name . '_selectedUser';
                $filePath = implode('.', $imageNameArray);
                imagegif($selectedDestImage, $filePath);
            }
        } else
        {
            imagegif($destImage);
            // Creating the selected version of the image at the same time means that the images must always be written to file.
            // So this whole else statment can't really be used!!
            /*if ($selected)
            {
              imagegif($selectedDestImage);
            }*/
        }
        imagedestroy($image);
        imagedestroy($destImage);
        unset($image, $destImage);
        if ($selected)
        {
            imagedestroy($selectedImage);
            imagedestroy($selectedDestImage);
            unset($selectedImage, $selectedDestImage);
        }
        unset($paramArray, $s, $realWidth, $realHeight, $srcWidth, $srcHeight, $white, $black, $charcoal, $orange, $red, $grey, $yellow, $pink, $brown, $edge);
        unset($itemCount, $total, $percentMulti, $degMulti, $degBegin);
    }

    /**
     * draws a pie chart image
     *
     * @param array $paramArray
     * @param boolean $toFile
     * @param string $filePath
     */
    public static function createPieChart($paramArray, $toFile = false, $filePath = null, $selected = false, $height = 35, $width = 35 )
    {
        $s = 8;
        $realWidth = $width;
        $realHeight = $height;
        $srcWidth = $realWidth * $s;
        $srcHeight = $realHeight * $s;

        $center = ceil($realWidth/2); // could use $realHeight instead as the image should always be square
        $arc = $realWidth - 2; // could use $realHeight instead as the image should always be square

        // create the larger source image
        $image = imagecreatetruecolor($srcWidth, $srcHeight);
        // create the real/final image
        $destImage = imagecreatetruecolor($realWidth, $realHeight);

        imagesetthickness($image, 8);
        imageantialias($image, true);
        // allocate some colours
        $white = imagecolorallocate($image, 255, 255, 255);
        $black = imagecolorallocate($image, 0, 0, 0);
        $charcoal = imagecolorallocate($image, 40, 40, 40);
        $orange = imagecolorallocate($image, 0xef, 0x64, 0x21);

        $grey = imagecolorallocate($image, 207, 207, 207);
        $yellow = imagecolorallocate($image, 227, 209, 57);
        $pink = imagecolorallocate($image, 203, 154, 169);
        $brown = imagecolorallocate($image, 130, 84, 54);

        //$noBand = imagecolorallocate($image, 0xf2, 0xd0, 0xab);
        $noBand = imagecolorallocate($image, 0x00, 0xcc, 0x66);
        $oneBand = imagecolorallocate($image, 0xff, 0x82, 0x00);
        //$fiveBand = imagecolorallocate($image, 0xa6, 0x8f, 0x76);
        $fiveBand = imagecolorallocate($image, 0xcc, 0xcc, 0xcc);

        imagecolortransparent($image, $black);
        imagecolortransparent($destImage, $black);

        if($selected)
        {
            // create the larger source image
            $selectedImage = imagecreatetruecolor($srcWidth, $srcHeight);
            // create the real/final image
            $selectedDestImage = imagecreatetruecolor($realWidth, $realHeight);

            imagesetthickness($selectedImage, 8);
            imageantialias($selectedImage, true);
            // allocate some colours
            $whiteSelected = imagecolorallocate($selectedImage, 255, 255, 255);
            $blackSelected = imagecolorallocate($selectedImage, 0, 0, 0);
            $charcoalSelected = imagecolorallocate($selectedImage, 40, 40, 40);
            $orangeSelected = imagecolorallocate($selectedImage, 0xef, 0x64, 0x21);
            $redSelected = imagecolorallocate($selectedImage, 0xcc, 0x00, 0x00);

            $greySelected = imagecolorallocate($selectedImage, 207, 207, 207);
            $yellowSelected = imagecolorallocate($selectedImage, 227, 209, 57);
            $pinkSelected = imagecolorallocate($selectedImage, 203, 154, 169);
            $brownSelected = imagecolorallocate($selectedImage, 130, 84, 54);

            //$noBand = imagecolorallocate($image, 0xf2, 0xd0, 0xab);
            $noBandSelected = imagecolorallocate($selectedImage, 0x00, 0xcc, 0x66);
            $oneBandSelected = imagecolorallocate($selectedImage, 0xff, 0x82, 0x00);
            //$fiveBand = imagecolorallocate($image, 0xa6, 0x8f, 0x76);
            $fiveBandSelected = imagecolorallocate($selectedImage, 0xcc, 0xcc, 0xcc);

            imagecolortransparent($selectedImage, $blackSelected);
            imagecolortransparent($selectedDestImage, $blackSelected);
        }

        // create the pie chart
        $notSnails = $paramArray['other'];
        unset($paramArray['other']);
        $itemCount = count($paramArray);
        //asort($paramArray);
        $total = array_sum($paramArray);
        if ($total > 0)
        {
            $percentMulti = 100 / $total;
            $degMulti = 360 / 100;
            $degBegin = 0;
            $i = 0;
            $runningTotal = 0;
            foreach ($paramArray as $colourVariable => $amount)
            {
                $runningTotal += $amount;
                if($runningTotal == $total)
                {
                    imagefilledarc($image, $center * $s, $center * $s, $arc * $s, $arc * $s, $degBegin, 360, ${ $colourVariable }, IMG_ARC_PIE);
                    if($selected)
                    {
                        imagefilledarc($selectedImage, $center * $s, $center * $s, $arc * $s, $arc * $s, $degBegin, 360, ${ $colourVariable.'Selected' }, IMG_ARC_PIE);
                    }
                    break;
                }
                elseif($amount > 0)
                {
                    $degAmount = floor(($amount * $percentMulti * $degMulti) + $degBegin);
                    imagefilledarc($image, $center * $s, $center * $s, $arc * $s, $arc * $s, $degBegin, $degAmount, ${ $colourVariable }, IMG_ARC_PIE);
                    if($selected)
                    {
                        imagefilledarc($selectedImage, $center * $s, $center * $s, $arc * $s, $arc * $s, $degBegin, $degAmount, ${ $colourVariable.'Selected' }, IMG_ARC_PIE);
                    }
                    $degBegin = $degAmount;
                }
            }
        }
        else
        {
            if ($notSnails > 0)
            {
                // only other species at this location
                imagefilledarc($image, $center * $s, $center * $s, $arc * $s, $arc * $s, 0, 360, $grey, IMG_ARC_PIE);
                if($selected)
                {
                    imagefilledarc($selectedImage, $center * $s, $center * $s, $arc * $s, $arc * $s, 0, 360, $greySelected, IMG_ARC_PIE);
                }
            } else
            {
                // no records at all for this location!!
                imagefilledarc($image, $center * $s, $center * $s, $arc * $s, $arc * $s, 0, 360, $white, IMG_ARC_PIE);
                if($selected)
                {
                    imagefilledarc($selectedImage, $center * $s, $center * $s, $arc * $s, $arc * $s, 0, 360, $whiteSelected, IMG_ARC_PIE);
                }
            }
        }
        imagearc($image, $center * $s, $center * $s, $arc * $s, $arc * $s, 0, 359, $charcoal);
        imagearc($image, $center * $s, $center * $s, $arc * $s, $arc * $s, 359, 360, $charcoal);

        // now the picture is finished, do the shrink...
        imagecopyresampled($destImage, $image, 0, 0, 0, 0, $realWidth, $realHeight, $srcWidth, $srcHeight);

        if($selected)
        {
            imagearc($selectedImage, $center * $s, $center * $s, $arc * $s, $arc * $s, 0, 359, $redSelected);
            imagearc($selectedImage, $center * $s, $center * $s, $arc * $s, $arc * $s, 359, 360, $redSelected);
            imagegammacorrect($selectedImage, 1.0, 0.5);

            // now the picture is finished, do the shrink...
            imagecopyresampled($selectedDestImage, $selectedImage, 0, 0, 0, 0, $realWidth, $realHeight, $srcWidth, $srcHeight);
        }

        // flush image
        if ($toFile)
        {
            //$imageName = SF_ROOT_DIR.DIRECTORY_SEPARATOR.'web'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'charts'.DIRECTORY_SEPARATOR.$fileName . '.gif';
            imagegif($destImage, $filePath);
            if ($selected)
            {
                $imageNameArray = explode('.', $filePath);
                $imageNameArray[count($imageNameArray) - 2] = $imageNameArray[count($imageNameArray) - 2] . '_selected';
                $filePath = implode('.', $imageNameArray);
                imagegif($selectedDestImage, $filePath);
            }
        } else
        {
            imagegif($destImage);
            // Creating the selected version of the image at the same time means that the images must always be written to file.
            // So this whole else statment can't really be used!!
            /*if ($selected)
            {
              imagegif($selectedDestImage);
            }*/
        }
        imagedestroy($image);
        imagedestroy($destImage);
        unset($image);
        unset($destImage);
        if($selected)
        {
            imagedestroy($selectedImage);
            imagedestroy($selectedDestImage);
            unset($selectedImage);
            unset($selectedDestImage);
        }
    }
}