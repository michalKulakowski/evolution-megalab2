<?php
namespace Library\Pins;


class Export
{
    protected $_data = [];

    protected $_headers = [
        'Event ID',
        'Date event record created',
        'Username',
        'Record name',
        'Comment',
        'Habitat',
        'Latitude',
        'Longitude',
        'Date',
        'Common Name',
        'Binomial Name',
        'Total',
        'Pink Unbanded',
        'Pink 1 Band',
        'Pink Many Bands',
        'Yellow Unbanded',
        'Yellow 1 Band',
        'Yellow Many Bands',
        'Brown Unbanded',
        'Brown 1 Band',
        'Brown Many Bands'
    ];

    protected $_types = [
        'nemoralis' => [
            'shortcut' => 'cn',
            'common_name' => 'Brown-lipped snail',
            'binomial_name' => 'Cepaea nemoralis'
        ],
        'hortensis' => [
            'shortcut' => 'ch',
            'common_name' => 'White-lipped snail',
            'binomial_name' => 'Cepaea hortensis'
        ],
    ];

    public function __construct($data)
    {
        $this->_data = $data;
    }

    public function toCSV()
    {
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="location.csv";');

        // open the "output" stream
        // see http://www.php.net/manual/en/wrappers.php.php#refsect2-wrappers.php-unknown-unknown-unknown-descriptioq
        $f = fopen('php://output', 'w');
        fputcsv($f, $this->_headers);

        foreach ($this->_types as $type => $info) {
            if (!empty($this->_data[$type])){
                $row = [
                    @$this->_data['record']['eventId'].'_'.$info['shortcut'],
                    @$this->_data['record']['recored_by']['date'],
                    @$this->_data['record']['recored_by']['user'],
                    @$this->_data['record']['name'],
                    @$this->_data['record']['comment'],
                    @$this->_data['record']['habitat_type'],
                    @$this->_data['record']['lat'],
                    @$this->_data['record']['lng'],
                    @date('Y-m-d', strtotime($this->_data['record']['recored_by']['date'])),
                    $info['common_name'],
                    $info['binomial_name'],
                    @$this->_data[$type]['total']
                ];
                foreach (['pink','yellow','brown'] as $color){
                    foreach ([0,1,5] as $band){
                        $row[] = $this->_findColorBandNum($this->_data[$type]['info'],$color,$band);
                    }
                }
                fputcsv($f, $row);
            }
        }
    }

    protected function _findColorBandNum($data, $color, $band)
    {
        $label = $this->_getLabelForSpot($color, $band);

        foreach ($data as $info) {
            if($info['label'] == $label)
                return $info['value'];
        }

        return 0;
    }

    protected function _getLabelForSpot($color, $band)
    {
        $colour = ucfirst(strtolower($color));
        switch ($band){
            case 0: return $colour .' with 0 bands'; break;
            case 1: return $colour .' with 1 band'; break;
            case 5: return $colour .' with many bands'; break;
        }
    }
}