<?php


namespace Library\Pins;


class ColourCount
{
    public static function countAllColourSpots($info)
    {
        $eventTotal = array('yellow' => 0, 'brown' => 0, 'pink' => 0);
        foreach ($info as $colour => $data){
            $eventTotal[$colour] = array_sum($data);
        }

        return $eventTotal;
    }

    public static function countAllColourSpotsPercentage($info)
    {
        $eventTotal = self::countAllColourSpots($info);

        $total = array_sum($eventTotal);
        if($total > 0){
            return array(
                'yellow' => ($eventTotal['yellow'] / $total) * 100,
                'brown' => ($eventTotal['brown'] / $total) * 100,
                'pink' => ($eventTotal['pink'] / $total) * 100,
                'other' => 0
            );
        }else{
            return array(
                'yellow' => 0,
                'brown' => 0,
                'pink' => 0,
                'other' => 0
            );
        }
    }
}