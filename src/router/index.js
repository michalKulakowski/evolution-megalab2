import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/current'
  },
  {
    path: '/current/:lat?/:lng?',
    name: 'current',
    component: Home
  },
  {
    path: '/historic/:lat?/:lng?',
    name: 'historic',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "historic" */ '../views/HistoricRecords.vue')
  },
  {
    path: '/new',
    name: 'Add new Records',
    component: () => import(/* webpackChunkName: "addNew" */ '../views/AddRecords.vue')
  },
  {
    path: '/bookmarks',
    name: 'Bookmarks',
    component: () => import(/* webpackChunkName: "addNew" */ '../views/Bookmarks.vue')
  },
  {
    path: '/instructions',
    name: 'Instruction',
    component: () => import(/* webpackChunkName: "instruction" */ '../views/Instruction.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
