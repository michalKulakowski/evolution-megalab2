import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    map: {
      zoom: 5,
      level: 1,
      bounds: {
        south: 44.70184002600227,
        west: -21.171392460449226,
        north: 58.178260347885214,
        east: 19.962866789253493
      },
      centerPosition: {
        lat: 52.0406575,
        lng: -0.763258
      }
    },
    app: {
      title: 'S317 Evolution Megalab',
      drawer: false,
      color: 'success',
      responsive: false,
      links: [
        {
          to: '/instructions',
          icon: 'mdi-newspaper-variant-outline',
          text: 'Instructions'
        },
        {
          to: '/current',
          icon: 'mdi-radiobox-marked',
          text: 'Current Records'
        },
        {
          to: '/historic',
          icon: 'mdi-pillar',
          text: 'Historic Records'
        },
        {
          to: '/new',
          icon: 'mdi-map-marker-plus',
          text: 'Add new Records'
        },
        {
          to: '/bookmarks',
          icon: 'mdi-bookmark-multiple',
          text: 'Bookmarks'
        }
      ]
    },
    user: {
      userId: null,
      isAdmin: false,
      username: null,
      fullname: null,
      isLogged: false
    }
  },
  mutations: {
    setCenterPosition: (state, value) => {
      state.map.centerPosition = value
    },
    setResponsive: (state, value) => {
      state.app.responsive = value
    },
    setDrawer: (state, value) => {
      state.app.drawer = value
    },
    setUserName: (state, value) => {
      state.user.username = value
    },
    setUserId: (state, value) => {
      state.user.userId = value
    },
    setIsAdmin: (state, value) => {
      state.user.isAdmin = value
    },
    setUserFullName: (state, value) => {
      state.user.fullname = value
    },
    setUserIsLogged: (state, value) => {
      state.user.isLogged = value
    },
    setZoom: (state, value) => {
      state.map.zoom = value
    },
    setMapLevel: (state, value) => {
      state.map.level = value
    },
    setMapBounds: (state, value) => {
      state.map.bounds.south = value.south
      state.map.bounds.west = value.west
      state.map.bounds.north = value.north
      state.map.bounds.east = value.east
    }
  },
  actions: {
    setUser ({ commit }, user) {
      commit('setUserId', parseInt(user.userId))
      commit('setIsAdmin', user.isAdmin)
      commit('setUserName', user.username)
      commit('setUserFullName', user.fullname)
      commit('setUserIsLogged', user.isLogged)
    },
    onResponsiveInverted ({ commit }) {
      if (window.innerWidth < 991) {
        commit('setResponsive', true)
      } else {
        commit('setResponsive', false)
      }
    },
    changeFavoriteStatus ({ commit, getters, state }, { eventId, scope }) {
      return new Promise((resolve, reject) => {
        Vue.axios.post('/api/favoriteEvent.php', {
          eventId,
          scope
        }).then(res => {
          if (res.data.status === 200) {
            resolve(res.data)
          } else {
            reject(res.data)
          }
        }).catch(error => {
          reject(error)
        })
      })
    },
    setMapZoom ({ commit }, zoom) {
      commit('setZoom', zoom)
      switch (zoom) {
        case 0:
        case 1:
        case 2:
          commit('setMapLevel', 0); break
        case 3:
        case 4:
        case 5:
          commit('setMapLevel', 1); break
        case 6:
        case 7:
          commit('setMapLevel', 2); break
        case 8:
        case 9:
          commit('setMapLevel', 3); break
        case 10:
        case 11:
        case 12:
          commit('setMapLevel', 4); break
        default:
          commit('setMapLevel', 5); break
      }
    }
  },
  modules: {
  },
  getters: {
    getAppTitle: state => {
      return state.app.title
    },
    getCenterPosition: state => {
      return state.map.centerPosition
    },
    isResponsive: state => {
      return state.app.responsive
    },
    getAppLinks: state => {
      return state.app.links
    },
    getMapBounds: state => {
      return state.map.bounds
    },
    getMapLevel: state => {
      return state.map.level
    },
    getMapZoom: state => {
      return state.map.zoom
    },
    getUserId: state => {
      return state.user.userId
    },
    getUserIsAdmin: state => {
      return state.user.isAdmin
    },
    getUserName: state => {
      return state.user.username
    },
    getUserFullName: state => {
      return state.user.fullname
    },
    checkIfUserIsLogged: state => {
      return state.user.isLogged
    }
  }
})
