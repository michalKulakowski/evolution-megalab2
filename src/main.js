import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import * as VueGoogleMaps from 'vue2-google-maps'
import axios from 'axios'
import VueAxios from 'vue-axios'

if (location.protocol !== 'https:' && require('./../config/app.config.json').enableHTTPSRedirect) {
  location.replace(`https:${location.href.substring(location.protocol.length)}`)
}

Vue.use(VueAxios, axios)
Vue.axios.defaults.baseURL = process.env.VUE_APP_BASE_URL
Vue.axios.defaults.headers['Content-Type'] = 'application/x-www-form-urlencoded'
Vue.axios.defaults.timeout = 0

Vue.use(VueGoogleMaps, {
  load: {
    key: require('./../config/app.config.json').googleMapApiKey, // Google Maps API KEY
    libraries: 'places' // This is required if you use the Autocomplete plugin
  }
  // installComponents: true,
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
