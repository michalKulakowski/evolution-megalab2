<?php
require __DIR__ . '/../../app.php';

$model = new \Model\Points();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $data = json_decode(file_get_contents("php://input"), true);
    $eventId = @$data['eventId'];

    try {
        $deleteModel = new \Model\DeleteRecord();
        $deleteModel->deleteEvent((int)$eventId);

        echo json_encode(['status' => 200, 'body' => ['msg' => 'Your record has ben deleted.']]);
    }catch (\Exception $ex){
        echo json_encode(['status' => 200, 'body' => ['msg' => $ex->getMessage()]]);
    }
}else{
    echo json_encode(['status' => 400, 'body' => ['msg' => 'Only POST request allowed!']]);
}