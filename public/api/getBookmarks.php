<?php
require __DIR__ . '/../../app.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    try{
        $data = json_decode(file_get_contents("php://input"), true);
        $filter = @$data['filter'];
        $limit = (@$data['limit'])?(int)$data['limit']:50;
        $offset = (@$data['offset'])?(int)$data['offset']:0;

        $bookmarkModel = new \Model\Bookmarks();

        $res = $bookmarkModel->getBookmarks($limit, $offset, $filter);

        echo json_encode(['status' => 200, 'body' => ['events' => $res]]);
    }catch (\Exception $ex){
        echo json_encode(['status' => 400, 'body' => ['msg' => $ex->getMessage()]]);
    }
} else {
    echo json_encode(['status' => 400, 'body' => ['msg' => 'Only POST request allowed!']]);
}