<?php
require __DIR__ . '/../../app.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    try{
        $data = json_decode(file_get_contents("php://input"), true);
        $eventId = $data['eventId'];
        $scope = $data['scope'];

        $newRecordModel = new \Model\Points();

        $res = $newRecordModel->changeFavoriteStatus($eventId, $scope);

        echo json_encode(['status' => 200, 'body' => ['msg' => $res]]);
    }catch (\Exception $ex){
        echo json_encode(['status' => 400, 'body' => ['msg' => $ex->getMessage()]]);
    }
} else {
    echo json_encode(['status' => 400, 'body' => ['msg' => 'Only POST request allowed!']]);
}