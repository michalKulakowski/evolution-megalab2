<?php
require __DIR__ . '/../../app.php';

$eventId = @$_GET["eventId"];
if(empty($eventId) || !is_numeric($eventId)){
    echo json_encode(['status' => 400, 'body' => ['msg' => 'EventId is required!']]);
    die();
}
$scope = @$_GET['scope'];
if(empty($scope) || $scope == 'current'){
    $model = new \Model\Points();
}elseif ($scope == 'historic'){
    $model = new \Model\Historic\Points();
}else{
    echo json_encode(['status' => 400, 'body' => ['msg' => 'Incorrect scope!']]);
    die();
}
$format = @$_GET["format"];

$pointInfo = $model->getPointInfo($eventId);

if($format == 'csv'){
    $export = new \Library\Pins\Export($pointInfo);

    $export->toCSV();
}else {
    echo json_encode(['status' => 200, 'body' => $pointInfo]);
}

