<?php
require __DIR__ . '/../../app.php';

$model = new \Model\Habitat();

echo json_encode(['status' => 200, 'body' => $model->getHabitatTypes()]);