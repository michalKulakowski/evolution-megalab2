<?php
namespace Model;

use Library\Map\Exception\EmptyClusterException;
use Library\Map\Grids;

class DeleteRecord extends RecordManager
{
    public function deleteEvent($eventId)
    {
        $this->startTransaction();

        try{
            $event = $this->getOne('
                    SELECT e.*, l.latitude, l.longitude
                    FROM event e
                    JOIN location l ON e.location_id = l.id
                    WHERE e.id = '.(int)$eventId);
            if(empty($event))
                throw new \Exception('Event with id: '.$eventId.' not exist');

            $this->_canDelete($event);

            $this->delete('DELETE FROM event WHERE id = '.(int)$event->id);
            $this->delete('DELETE FROM location WHERE id = '.(int)$event->location_id);
            $this->delete('DELETE FROM spot WHERE event_id = '.(int)$event->id);

            $this->_deleteEventFromCluster($event->latitude, $event->longitude);

            $this->commit();
            $this->_moveClusterFiles();
            $this->_deleteEventPin($event->id);
        }catch (\Exception $ex){
            $this->rollback();
            $this->_removeFiles();
        }
    }

    protected function _canDelete($event)
    {
        $user = new User();

        if($user->isAdmin())
            return true;

        if ($user->getUserId() != $event->created_by)
            throw new \Exception('You don\'t have permission to delete this event');

        return true;
    }

    protected function _deleteEventFromCluster($lat, $lng)
    {
        $level = Grids::FINEST_LEVEL;
        $data = [];
        do{
            $y = Grids::calculateGridCoordinate($lat, Grids::getSize($level));
            $x = Grids::calculateGridCoordinate($lng, Grids::getSize($level));
            $cluster = $this->_clusterModel->findCluster($level, $y, $x);

            if(!empty($cluster)){
                try{
                    $clusterImgPath = $this->_getClusterPathInTmp($cluster);
                    $this->_clusterModel->rebuildClusterImage($cluster, $clusterImgPath);
                }catch (EmptyClusterException $ex) {
                    $this->_deleteCluster($cluster);
                }

                $lat = $cluster->latitude;
                $lng = $cluster->longitude;
            }
            $level--;
        }while($level >= 0);
    }

    protected function _deleteCluster($cluster)
    {
        $this->delete('DELETE FROM map_cluster WHERE id = '.(int)$cluster->id);

        $fileName = $this->_clusterModel->getImageName($cluster);
        $url = CLUSTER_DIR . '/'.$fileName;
        if(file_exists($url))
            unlink($url);

        $fileName = $this->_clusterModel->getImageName($cluster, true);
        $url = CLUSTER_DIR . '/'.$fileName;
        if(file_exists($url))
            unlink($url);
    }

    protected function _deleteEventPin($eventId)
    {
        $userFileName = 'pin'.$eventId.'_User.gif';
        $fileName = 'pin'.$eventId.'.gif';
        $url = PINS_DIR . '/'.$fileName;
        if(file_exists(PINS_DIR . '/'.$userFileName))
            unlink(PINS_DIR . '/'.$userFileName);
        if (file_exists($url))
            unlink($url);

        $userFileName = 'pin'.$eventId.'_selectedUser.gif';
        $fileName = 'pin'.$eventId.'_selected.gif';
        $url = PINS_DIR . '/'.$fileName;
        if(file_exists(PINS_DIR . '/'.$userFileName))
            unlink(PINS_DIR . '/'.$userFileName);
        if (file_exists($url))
            unlink($url);
    }
}