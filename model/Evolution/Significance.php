<?php
namespace Model\Evolution;


use Library\MySQL\Model;
use Model\Event;

class Significance extends Model
{
    protected $_binomialName;
    protected $_eventId;
    protected $_historicEventId;

    protected $_historicalPeriodId;
    protected $_distance;

    protected $_bandP;
    protected $_bandGvalue;

    protected $_colourP;
    protected $_colourGvalue;
    protected $_baseTexts = [];

    public function __construct($eventId, $historicEventId, $historicalPeriodId, $distance, $binomialName)
    {
        $this->_eventId = $eventId;
        $this->_historicEventId = $historicEventId;
        $this->_binomialName = $binomialName;
        $this->_historicalPeriodId = $historicalPeriodId;
        $this->_distance = $distance;
        $this->_prepareText();

        foreach (array('band','colour') as $category){
            $rows = $this->getValuePerCategory($category);
            $data = $this->calculateGvalue($rows);

            $this->{'_'.$category.'Gvalue'} = $data['gValue'];
            $this->{'_'.$category.'P'} = $data['p'];
        }
        $this->createSignificanceText();
    }

    public function getColourSignificanceShortText()
    {
        return $this->sampleColourSignificanceShortText;
    }

    public function getBandSignificanceShortText()
    {
        return $this->sampleBandSignificanceShortText;
    }

    public function getValuePerCategory($category)
    {
        $eventModel = new Event();
        $historicEventModel = new \Model\Historic\Event();

        $spots = $eventModel->countAllSpots($this->_eventId, $this->_binomialName);
        $historicSpots = $historicEventModel->countAllSpots($this->_historicEventId, $this->_binomialName);

        $result = [];
        switch ($category)
        {
            case 'band' :
                $result[] = $this->_countBands($spots);
                $result[] = $this->_countBands($historicSpots);
                break;
            case 'colour' :
                $result[] = $this->_countColors($spots);
                $result[] = $this->_countColors($historicSpots);
                break;
        }

        return $result;
    }

    /* Calculates the G value for either the colour or
    * the number of bands of the snails sampled in the
    * current record compared to the historical record
    *
    * @param array $rows an array containing all the data needed to calculate the value
    * @return array contains the p number, the number of columns used to calculate, and the g value
    */
    function calculateGvalue($rows)
    {
        $rowTotals = array();
        $columnTotals = array('column1' => 0, 'column2' => 0, 'column3' => 0);
        $returnArray = array('p' => 0.06, 'numberOfColumns' => 3, 'gValue' => 0);

        foreach ($rows as $r)
        {
            $rowTotals[] = array_sum($r);
            $columnTotals['column1'] += $r['column1'];
            $columnTotals['column2'] += $r['column2'];
            $columnTotals['column3'] += $r['column3'];
        }

        $grandTotal = array_sum($rowTotals);

        $zeroColumnName = null;
        foreach ($columnTotals as $col => $ct) // check if any columnTotals are zero
        {
            if($ct === 0)
            {
                if(!is_null($zeroColumnName))
                {
                    //$returnArray['p'] = 0.06;
                    $returnArray['numberOfColumns'] = 1;
                    return $returnArray; // return if more than one columnTotal is zero
                    // i.e. only one columnTotal isn't zero
                }
                else
                {
                    $zeroColumnName = $col;
                }
            }
        }
        if(!is_null($zeroColumnName)) // remove column if the columnTotal is zero
        {
            foreach ($rows as &$r)
            {
                unset($r[$zeroColumnName]);
            }
            $returnArray['numberOfColumns'] = 2;
        }

        foreach ($rows as $num => $r) // calculate expected values return if any are under 5
        {
            if($zeroColumnName != 'column1')
            {
                $rowsExpected[$num]['column1'] = ($rowTotals[$num] * $columnTotals['column1']) / $grandTotal;
                if($rowsExpected[$num]['column1'] < 5)
                {
                    $returnArray['p'] = 0;
                    return $returnArray;
                }
            }
            if($zeroColumnName != 'column2')
            {
                $rowsExpected[$num]['column2'] = ($rowTotals[$num] * $columnTotals['column2']) / $grandTotal;
                if($rowsExpected[$num]['column2'] < 5)
                {
                    $returnArray['p'] = 0;
                    return $returnArray;
                }
            }
            if($zeroColumnName != 'column3')
            {
                $rowsExpected[$num]['column3'] = ($rowTotals[$num] * $columnTotals['column3']) / $grandTotal;
                if($rowsExpected[$num]['column3'] < 5)
                {
                    $returnArray['p'] = 0;
                    return $returnArray;
                }
            }
        }

        foreach ($rows as &$r) // resolve any zeros in the raw data
        {
            if(array_key_exists('column1', $r) && $r['column1'] == 0)
            {
                $r['column1'] = 0.001;
            }
            if(array_key_exists('column2', $r) && $r['column2']  == 0)
            {
                $r['column2'] = 0.001;
            }
            if(array_key_exists('column3', $r) && $r['column3']  == 0)
            {
                $r['column3'] = 0.001;
            }
        }


        $cellsTotal = 0;
        foreach ($rows as $num => &$r) // calculate raw number multiplied by
            // natural log of each raw number divided by the expected value
        {
            if(array_key_exists('column1', $r))
            {
                $r['column1'] = $r['column1'] * log($r['column1'] / $rowsExpected[$num]['column1']);
                $cellsTotal += $r['column1'];
            }
            if(array_key_exists('column2', $r))
            {
                $r['column2'] = $r['column2'] * log($r['column2'] / $rowsExpected[$num]['column2']);
                $cellsTotal += $r['column2'];
            }
            if(array_key_exists('column3', $r))
            {
                $r['column3'] = $r['column3'] * log($r['column3'] / $rowsExpected[$num]['column3']);
                $cellsTotal += $r['column3'];
            }
        }

        $returnArray['gValue'] = 2 * $cellsTotal;
        if(!is_null($zeroColumnName))
        {
            if($returnArray['gValue'] >= 9.210)
            {
                $returnArray['p'] = 0.01;
            }
            elseif($returnArray['gValue'] >= 5.991)
            {
                $returnArray['p'] = 0.05;
            }
            else
            {
                $returnArray['p'] = 0.06;
            }

        }
        else
        {
            if($returnArray['gValue'] >= 6.635)
            {
                $returnArray['p'] = 0.01;
            }
            elseif($returnArray['gValue'] >= 3.841)
            {
                $returnArray['p'] = 0.05;
            }
            else
            {
                $returnArray['p'] = 0.06;
            }
        }
        return $returnArray;
    }

    protected $sampleBandSignificanceText;
    protected $sampleBandSignificanceShortText;
    protected $sampleColourSignificanceText;
    protected $sampleColourSignificanceShortText;

    /**
     * Creates the text that describes the
     * significance of the current record with relation
     * to the historical record found
     *
     */
    function createSignificanceText()
    {
        if($this->_distance <= 0.5)
        {
            // set text for band
            if($this->_bandP == 0)
            {
                $this->sampleBandSignificanceText = $this->_baseTexts['bpSampleTooSmall'];
                $this->sampleBandSignificanceShortText = $this->_baseTexts['sampleTooSmallShort'];
            }
            elseif($this->_bandP <= 0.05)
            {
                $this->sampleBandSignificanceText = $this->_baseTexts['bpCloseAndHigh'];
                $this->sampleBandSignificanceShortText = $this->_baseTexts['closeAndHighShort'];
            }
            else
            {
                $this->sampleBandSignificanceText = $this->_baseTexts['bpCloseAndLow'];
                $this->sampleBandSignificanceShortText = $this->_baseTexts['closeAndLowShort'];
            }
            // set text for colour
            if($this->_colourP == 0)
            {
                $this->sampleColourSignificanceText = $this->_baseTexts['scSampleTooSmall'];
                $this->sampleColourSignificanceShortText = $this->_baseTexts['sampleTooSmallShort'];
            }
            elseif($this->_colourP <= 0.05)
            {
                $this->sampleColourSignificanceText = $this->_baseTexts['scCloseAndHigh'];
                $this->sampleColourSignificanceShortText = $this->_baseTexts['closeAndHighShort'];
            }
            else
            {
                $this->sampleColourSignificanceText = $this->_baseTexts['scCloseAndLow'];
                $this->sampleColourSignificanceShortText = $this->_baseTexts['closeAndLowShort'];
            }
        }
        else
        {
            // set text for band
            if($this->_bandP == 0)
            {
                $this->sampleBandSignificanceText = $this->_baseTexts['bpSampleTooSmall'];
                $this->sampleBandSignificanceShortText = $this->_baseTexts['sampleTooSmallShort'];
            }
            elseif($this->_bandP <= 0.05)
            {
                if($this->_colourP <= 0.01)
                {
                    $this->sampleBandSignificanceText = $this->_baseTexts['bpFarAndVeryHigh'];
                    $this->sampleBandSignificanceShortText = $this->_baseTexts['farAndVeryHighShort'];
                }
                else
                {
                    $this->sampleBandSignificanceText = $this->_baseTexts['bpFarAndHigh'];
                    $this->sampleBandSignificanceShortText = $this->_baseTexts['farAndHighShort'];
                }
            }
            else
            {
                $this->sampleBandSignificanceText = $this->_baseTexts['pbFarAndLow'];
                $this->sampleBandSignificanceShortText = $this->_baseTexts['farAndLowShort'];
            }
            // set text for colour
            if($this->_colourP == 0)
            {
                $this->sampleColourSignificanceText = $this->_baseTexts['scSampleTooSmall'];
                $this->sampleColourSignificanceShortText = $this->_baseTexts['sampleTooSmallShort'];
            }
            elseif($this->_colourP <= 0.05)
            {
                if($this->_colourP <= 0.01)
                {
                    $this->sampleColourSignificanceText = $this->_baseTexts['scFarAndVeryHigh'];
                    $this->sampleColourSignificanceShortText = $this->_baseTexts['farAndVeryHighShort'];
                }
                else
                {
                    $this->sampleColourSignificanceText = $this->_baseTexts['scFarAndHigh'];
                    $this->sampleColourSignificanceShortText = $this->_baseTexts['farAndHighShort'];
                }
            }
            else
            {
                $this->sampleColourSignificanceText = $this->_baseTexts['scFarAndLow'];
                $this->sampleColourSignificanceShortText = $this->_baseTexts['farAndLowShort'];
            }
        }
    }

    protected function _countColors($data)
    {
        $result = [];
        $i = 1;
        foreach ($data as $val){
            $result['column'.$i] = array_sum($val);
            $i++;
        }

        return $result;
    }

    protected function _countBands($data)
    {
        $bands = array(0 => 0, 1 => 0, 5 => 0);

        foreach ($data as $colour => $val){
            foreach ($val as $band => $num){
                $bands[(int)$band] += $num;
            }
        }

        $result = [];
        $i = 1;
        foreach ($bands as $val){
            $result['column'.$i] = $val;
            $i++;
        }
        return $result;
    }

    protected function _prepareText()
    {
        $this->_baseTexts['sampleTooSmallShort'] = 'The sample sizes were too small for a statistical comparison.';
        $this->_baseTexts['closeAndHighShort'] = 'It is highly probable evolution has taken place in the time between the earlier sample and the present one.';
        $this->_baseTexts['closeAndLowShort'] = 'The small change between the present sample and the earlier one may well have occurred by chance.'.' '.'This does not necessarily mean that natural selection has not affected snails in this population.';
        $this->_baseTexts['farAndVeryHighShort'] = 'The distance between the present sample and the earlier one is too great to infer probable evolution, although there is a highly significant difference.';
        $this->_baseTexts['farAndHighShort'] = 'The distance between the present sample and the earlier one is too great to infer probable evolution, although there is a significant difference.';

        $this->_baseTexts['farAndLowShort'] = 'The distance between the present sample and the earlier one is too great to infer possible evolution plus the change is small enough to have occurred by chance.';

        $tooSmall = 'This is due to the number of sampled snails being too small.';
        $this->_baseTexts['bpSampleTooSmall'] = 'A statistical comparison could not be made for the banding patterns.'.' '.$tooSmall;
        $this->_baseTexts['scSampleTooSmall'] = 'A statistical comparison could not be made for the shell colours.'.' '.$tooSmall;

        $this->_baseTexts['bpCloseAndHigh'] = 'The change in the frequencies of banding patterns indicates that evolution has probably taken place over the interval between the date of the earlier sample and your own.';
        $this->_baseTexts['scCloseAndHigh'] = 'The change in the frequencies of shell colours indicates that evolution has probably taken place over the interval between the date of the earlier sample and your own.';

        $ns = 'This does not necessarily mean that natural selection has not affected snails in this population.';
        $this->_baseTexts['bpCloseAndLow'] = 'The difference in the frequencies of banding patterns between your sample and the earlier one nearby is relatively small and may have occurred by chance.'.' '.'If snail banding patterns are adapted to local conditions and these have not changed, then natural selection will have preserved the beneficial patterns because, for example, they provide camouflage.';
        $this->_baseTexts['scCloseAndLow'] = 'The difference in the frequencies of shell colours between your sample and the earlier one nearby is relatively small and may have occurred by chance.'.' '.'If snail shell colours are adapted to local conditions and these have not changed, then natural selection will have preserved the beneficial colours because, for example, they provide camouflage.';

        $this->_baseTexts['bpFarAndHigh'] = 'The difference in the frequencies of banding patterns between the earlier sample and your own is significant.';
        $this->_baseTexts['scFarAndHigh'] = 'The difference in the frequencies of shell colours between the earlier sample and your own is significant.';
        $this->_baseTexts['bpFarAndVeryHigh'] = 'The difference in the frequencies of banding patterns between the earlier sample and your own is highly significant.';
        $this->_baseTexts['scFarAndVeryHigh'] = 'The difference in the frequencies of shell colours between the earlier sample and your own is highly significant.';
        $FHend = 'But, because the two samples were not very near each other, we cannot tell whether this difference represents a change over time, or is simply a difference caused by adaptation to local conditions (e.g. the height of the vegetation) in the two different localities.';

        $this->_baseTexts['bpFarAndHigh'] .= ' '.$FHend;
        $this->_baseTexts['scFarAndHigh'] .= ' '.$FHend;
        $this->_baseTexts['bpFarAndVeryHigh'] .= ' '.$FHend;
        $this->_baseTexts['scFarAndVeryHigh'] .= ' '.$FHend;

        $this->_baseTexts['pbFarAndLow'] = 'The difference in the frequencies of banding patterns between your sample and the earlier one is relatively small.'.' '.$ns.' '.'If snail banding patterns are adapted to local conditions and these have not changed, then natural selection will have preserved the beneficial patterns because, for example, they provide camouflage.';
        $this->_baseTexts['scFarAndLow'] = 'The difference in the frequencies of shell colours between your sample and the earlier one is relatively small.'.' '.$ns.' '.'If snail shell colours are adapted to local conditions and these have not changed, then natural selection will have preserved the beneficial colours because, for example, they provide camouflage.';
    }
}