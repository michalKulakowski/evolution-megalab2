<?php
namespace Model;

use Library\MySQL\Model;

class Bookmarks extends Model
{
    protected $_userId;
    protected $_pointModel;
    protected $_historicPointModel;

    public function __construct()
    {
        $userModel = new User();
        $this->_userId = $userModel->getUserId();
    }

    public function getBookmarks($limit, $offset, $filter = null)
    {
        $query = 'SELECT q.event_id, q.created_at, q.`scope`
                FROM (';
        $favoriteAdded = false;
        if(empty($filter) || $filter == 'favorite'){
            $query .= 'SELECT q1.event_id, q1.`created_at`, q1.`scope` FROM `user_favorite` q1
                WHERE q1.user_id ='.$this->_userId;
            $favoriteAdded = true;
        }
        if(empty($filter) || $filter == 'created'){
            if($favoriteAdded)
                $query .= ' UNION ';
            $query .= 'SELECT q2.id as event_id, q2.`created_at`, "current" as `scope` FROM `event` q2
                WHERE q2.created_by = '.$this->_userId;
        }
        $query .= ') as q
            ORDER BY created_at
            LIMIT '.(int)$offset.', '.(int)$limit;

        $res = $this->query($query);
        $result = [];
        foreach ($res as $favorite){
            $event = $this->_getPointModel($favorite->scope)->getRecordData($favorite->event_id);
            if($event){
                $event['scope'] = $favorite->scope;
                $result[] = $event;
            }
        }
        return $result;
    }

    protected function _getFavorite()
    {
        $result = [];
        $res = $this->query('SELECT * FROM `user_favorite`
                WHERE user_id = '.$this->_userId);
        foreach ($res as $favorite){
            $event = $this->_getPointModel($favorite->scope)->getRecordData($favorite->event_id);
            if($event){
                $event['scope'] = $favorite->scope;
                $result[] = $event;
            }
        }
        return $result;
    }

    /**
     * @param string $scope
     * @return Historic\Points|Points
     */
    protected function _getPointModel($scope)
    {
        if($scope == 'historic'){
            if(empty($this->_historicPointModel))
                $this->_historicPointModel = new \Model\Historic\Points();
            return $this->_historicPointModel;
        }
        if(empty($this->_pointModel))
            $this->_pointModel = new \Model\Points();
        return $this->_pointModel;
    }
}