<?php


namespace Model;


use Library\MySQL\Model;
use Model\Species\SpeciesAbstract;

class Spots extends Model
{
    protected $_specie = null;
    protected $_eventId = null;

    protected $_prefix = '';

    public function setSpecies($obj)
    {
        $this->_specie = $obj;
    }

    public function setEventId($eventId)
    {
        $this->_eventId = $eventId;
    }

    public function getSpeciesInstantId($params)
    {
        if(isset($this->_specie) && $this->_specie->object_name && !empty($params)){
            $method = new \ReflectionMethod("Model\\Species\\".$this->_specie->object_name, 'getInstanceId');
            $id = $method->invoke(null, $params);
            unset($method);
            return $id;
        }
        return null;
    }

    public function addSpot($paramArray, $howMany, $userId)
    {
        if(empty($paramArray))
            return null;

        $instanceId = $this->getSpeciesInstantId($paramArray);

        for($i = 0; $i < $howMany; $i++) {
            $this->insert('spot', [
                'species_id' => $this->_specie->id,
                'object_instance_id' => $instanceId,
                'event_id' => $this->_eventId,
                'sf_guard_user_id' => $userId,
                'created_at' => date('Y-m-d H:i:s')
            ]);
        }

        return true;
    }

    public function countSpots($paramArray = null, $userId = null)
    {
        if (!isset($this->_specie))
            throw new \Exception('Species object is required!');
        if (!isset($this->_eventId))
            throw new \Exception('Event Id is required!');

        $instanceId = $this->getSpeciesInstantId($paramArray);

        $query = "SELECT COUNT(id) AS spots
              FROM ".$this->_prefix."spot
              WHERE species_id = ".$this->_specie->id."
              AND ".$this->_prefix."event_id = ".$this->_eventId;

        if($userId)
        {
            $query .= " AND sf_guard_user_id = ".$userId;
        }

        if($instanceId)
        {
            $query .= " AND object_instance_id = ".$instanceId;
        }

        $res = $this->getOne($query);

        if(!empty($res))
            return $res->spots;

        return 0;
    }

    function __destruct() {
        unset($this->_eventId);
        unset($this->_specie);
    }
}