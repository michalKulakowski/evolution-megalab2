<?php


namespace Model\Historic;


class Cluster extends \Model\Cluster
{
    protected $_prefix = 'historic_';

    public function getPathToCluster($cluster)
    {
        $result = array();
        $fileName = $this->getImageName($cluster);
        $url = CLUSTER_DIR . '/'.$fileName;
        if(file_exists($url))
            $result['url'] = '/assets/historic/cluster/'.$fileName;

        $fileName = $this->getImageName($cluster, true);
        $url = CLUSTER_DIR . '/'.$fileName;
        if(file_exists($url))
            $result['selectedUrl'] = '/assets/historic/cluster/'.$fileName;

        return $result;
    }
}