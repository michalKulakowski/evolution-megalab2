<?php
namespace Model;
use Library\MySQL\Model;

class User extends Model
{
    protected $_username = null;
    protected $_userId = null;
    protected $_fullName = null;
    protected $_isAdmin = null;
    protected $_defaultPass = '5f3ab4405d4765.68364932';

    public function __construct()
    {
        if(isset($_SERVER['HTTP_SAMS_USER']) && !empty($_SERVER['HTTP_SAMS_USER']))
            $this->_username = $_SERVER['HTTP_SAMS_USER'];

        if(isset($_COOKIE['HS7BDF']) && !empty($_COOKIE['HS7BDF']))
            $this->_fullName = $_COOKIE['HS7BDF'];

        if($this->checkIfLogged())
            $this->_userId = $this->getUserId();

    }

    public function checkIfLogged()
    {
        return (!empty($this->_username));
    }

    public function getUsername()
    {
        return $this->_username;
    }

    public function getFullName()
    {
        return $this->_fullName;
    }

    public function isAdmin()
    {
        if($this->_isAdmin === null)
            $this->_loadUser();

        return (bool)$this->_isAdmin;
    }

    public function getUserId()
    {
        if(empty($this->_userId))
            $this->_loadUser();

        return $this->_userId;
    }

    public function addNewUser()
    {
        $salt = uniqid('', true);
        return $this->insert('sf_guard_user', [
            'username' => $this->_username,
            'algorithm' => 'sha1',
            'salt' => $salt,
            'password' => sha1($salt.$this->_defaultPass),
            'created_at' => date('Y-m-d H:i:s'),
            'is_active' => 1,
            'is_super_admin' => 0
        ]);
    }

    protected function _loadUser()
    {
        if(!$this->checkIfLogged()){
            throw new \Exception('You are need to be logged');
        }

        $res = $this->getOne('SELECT * FROM sf_guard_user WHERE `username` = "'.$this->_escape($this->_username).'"');

        if(!empty($res)){
            $this->_userId = $res->id;
            $this->_isAdmin = (bool)$res->is_super_admin;
        } else {
            $this->_userId = $this->addNewUser();
            $this->_isAdmin = false;
        }

        return true;
    }
}