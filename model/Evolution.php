<?php
namespace Model;


use Library\Map\Grids;
use Library\MySQL\Model;
use Model\Evolution\Significance;

class Evolution extends Model
{
    protected $_event;
    protected $_species = null;

    protected $_historicalPresent = false;
    protected $_historicalPeriodId;
    protected $_closestHistoricalId;
    protected $_distance;


    public function getData($eventId, $binomialName)
    {
        $result = null;
        $this->setEvent($eventId);
        $this->setSpecies($binomialName);

        $this->findPastEventClosestToCurrent();

        if($this->_historicalPresent){
            $result = array(
                'title' => 'Comparison with past results for '. $binomialName,
                'distance' => $this->_getDistanceText()
            );

            $result['spots'] = $this->getSpotsData($eventId, $binomialName);

            $significanceModel = new Significance(
                $eventId,
                $this->_closestHistoricalId,
                $this->_historicalPeriodId,
                $this->_distance,
                $binomialName
            );

            $result['color_significance'] = $significanceModel->getColourSignificanceShortText();
            $result['band_significance'] = $significanceModel->getBandSignificanceShortText();
        }

        return $result;
    }

    public function setEvent($eventId)
    {
        $res = $this->getOne('
            SELECT e.*, l.`latitude`, l.`longitude`, l.habitat_id
            FROM `event` e 
            JOIN `location` l ON e.`location_id` = l.id
            WHERE
            e.id = '.(int)$this->_escape($eventId).'
            ');

        if(!empty($res)){
            $this->_event = $res;
        }else{
            $this->_event = null;
        }
    }

    public function setSpecies($obj)
    {
        if(is_object($obj))
            $this->_species = $obj;
        else{
            $this->_species = $this->getOne(
                'SELECT * FROM `species` WHERE `binomial_name` = "'.$this->_escape($obj).'"'
            );
        }
    }

    public function getSpotsData($eventId, $binomialName)
    {
        $result = array('current' => array(), 'historic' => array());

        $eventModel = new Event();
        $historicEventModel = new \Model\Historic\Event();

        $spots = $eventModel->countAllSpots($eventId, $binomialName);
        $result['current'] = array(
            'title' => 'Now',
            'data' => array(
                'color' => $this->_countColours($spots),
                'band' => $this->_countBands($spots)
            )
        );

        $spots = $historicEventModel->countAllSpots($this->_closestHistoricalId, $binomialName);
        $result['historic'] = array(
            'title' => $this->_createPeriodText(),
            'data' => array(
                'color' => $this->_countColours($spots),
                'band' => $this->_countBands($spots)
            )
        );

        return $result;
    }

    /**
     * looks to see if there is a historical record
     * within a 5km square of the current record
     *
     */
    public function findPastEventClosestToCurrent()
    {
        if(empty($this->_event))
            throw new \Exception('Event is required');

        $gridBox = Grids::get10kmGridBoxAroundPoint($this->_event->latitude, $this->_event->longitude);

        $habitatId = $this->_event->habitat_id;
        $closeEvents = $this->query('SELECT e.*, l.`latitude`, l.`longitude`
            FROM `historic_event` e 
            JOIN `historic_location` l ON e.`historic_location_id` = l.id and l.`habitat_id` = '.$habitatId.'
            WHERE
            l.`latitude` >= '.$gridBox['bottomLeft']['latitude'].'
            AND l.`latitude` <= '.$gridBox['topLeft']['latitude'].'
            AND l.`longitude` >= '.$gridBox['topRight']['longitude'].'
            AND l.`longitude` <= '.$gridBox['topLeft']['longitude']);

        if(count($closeEvents) > 0)
        {
            $distanceToCurrent = array();
            $spotModel = new Spots();
            $spotModel->setSpecies($this->_species);
            foreach($closeEvents as $e)
            {
                $spotModel->setEventId($e->id);
                if($spotModel->countSpots())
                {
                    // new way of finding the distance between the two coordinates... hopefully more accurate!
                    $distanceToCurrent[$e->id] = Grids::getDistanceBetweenTwoPoints(
                        $this->_event->latitude,
                        $this->_event->longitude,
                        $e->latitude,
                        $e->longitude
                    );
                }
            }

            if(count($distanceToCurrent) > 0)
            {
                asort($distanceToCurrent, SORT_NUMERIC);
                $this->_historicalPresent = true;
                $this->_closestHistoricalId = key($distanceToCurrent);
                $this->_distance = round(current($distanceToCurrent), 2);
            }
        }
        else
        {
            $this->_historicalPresent = false;
        }
    }

    protected function _getDistanceText()
    {
        return 'The historical record was '.$this->_distance.'km from your record.';
    }

    /**
     * Creates the correct text to represent the
     * period the the historical record was found,
     * also sets an id to represent this period
     *
     */
    protected function _createPeriodText()
    {
        $date = $this->_getHistoricDate();
        $datePrefix = substr($date, 0, 3);
        if($datePrefix == '198' || $datePrefix == '199' || substr($datePrefix, 0, 2) == '20')
        {
            $this->_historicalPeriodId = 3;
            return '1980-2000';
        }
        elseif($datePrefix == '197')
        {
            $this->_historicalPeriodId = 2;
            return '1970s';
        }
        elseif($datePrefix == '196')
        {
            $this->_historicalPeriodId = 1;
            return '1960s';
        }
        else
        {
            $this->_historicalPeriodId = 0;
            return 'before 1960';
        }
    }

    protected function _getHistoricDate()
    {
        $res = $this->getOne('SELECT `date` FROM `historic_event` WHERE id = '.$this->_closestHistoricalId);

        return (empty($res))? null: $res->date;
    }

    protected function _countBands($data)
    {
        $bands = array(0 => 0, 1 => 0, 5 => 0);

        foreach ($data as $colour => $val){
            foreach ($val as $band => $num){
                $bands[(int)$band] += $num;
            }
        }

        $result = array();
        if(!empty($bands[0])){
            $result[] = array('label' => 'Non bands', 'value' => $bands[0]);
        }
        if(!empty($bands[1])){
            $result[] = array('label' => 'One band', 'value' => $bands[1]);
        }
        if(!empty($bands[5])){
            $result[] = array('label' => 'Multiple bands', 'value' => $bands[5]);
        }

        return $result;
    }

    protected function _countColours($data)
    {
        $result = [];

        foreach ($data as $colour => $val){
            $result[] = array('label' => ucfirst(strtolower($colour)), 'value' => array_sum($val));
        }

        return $result;
    }
}