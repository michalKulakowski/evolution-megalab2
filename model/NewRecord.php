<?php
namespace Model;

use Library\Map\Grids;

class NewRecord extends RecordManager
{
    protected $_binomialName = [
        'hortensis' => 'Cepaea hortensis',
        'nemoralis' => 'Cepaea nemoralis'
    ];

    public function create($data)
    {
        $newRecord = $this->_validatePoint($data);

        $this->startTransaction();

        try{
            $locationId = $this->insert('location', [
                'name' => '',
                'latitude' => $newRecord['position']['lat'],
                'longitude' => $newRecord['position']['lng'],
                'zoom_level' => $newRecord['zoom'],
                'habitat_id' => $newRecord['habitat'],
                'created_by' => $this->_userId,
                'created_at' => date('Y-m-d H:i:s')
            ]);

            $eventId = $this->insert('event', [
                'name' => $newRecord['name'],
                'comment' => $newRecord['comment'],
                'date' => date('Y-m-d', strtotime($newRecord['date'])),
                'location_id' => $locationId,
                'created_by' => $this->_userId,
                'created_at' => date('Y-m-d H:i:s'),
                'email_sent' => 1
            ]);

            $this->_addSpots($eventId, $newRecord['spots']);

            $this->_createEventPin($eventId);

            $this->_addEventToCluster($newRecord['position']['lat'], $newRecord['position']['lng']);

            $this->commit();
            $this->_moveClusterFiles();
        }catch (\Exception $ex) {
            $this->rollback();
            $this->_removeFiles();
            throw $ex;
        }
    }

    protected function _createEventPin($eventId)
    {
        $eventsModel = new Event();
        $count = $eventsModel->countAllColourSpotsPercentage($eventId);

        if(array_sum($count) == 0){
            throw new \Exception("Event with id: $eventId is empty (no spots inside)");
        }
        $fileName = '/pin'.$eventId.'.gif';
        \Library\Pins\Images::createPieChartPin($count, true, true, PINS_DIR.$fileName);
        $this->_createdFiles['event'] = [
            PINS_DIR.'/pin'.$eventId.'.gif',
            PINS_DIR.'/pin'.$eventId.'_selected.gif',
            PINS_DIR.'/pin'.$eventId.'_User.gif',
            PINS_DIR.'/pin'.$eventId.'_selectedUser.gif',
        ];
    }

    protected function _removeFiles()
    {
        foreach ($this->_createdFiles['event'] as $file){
            if(file_exists($file))
                unlink($file);
        }
        parent::_removeFiles();
    }

    protected function _addEventToCluster($lat, $lng)
    {
        $level = Grids::FINEST_LEVEL;
        $data = [];
        do{
            $y = Grids::calculateGridCoordinate($lat, Grids::getSize($level));
            $x = Grids::calculateGridCoordinate($lng, Grids::getSize($level));
            $clusterId = $this->_clusterModel->createCluster($level, $y, $x);
            $cluster = $this->getOne('SELECT * FROM map_cluster WHERE id = '.(int)$clusterId);
            $data[] = $cluster;

            if(empty($cluster))
                throw new \Exception("Cluster with id: $clusterId not found.");

            $clusterImgPath = $this->_getClusterPathInTmp($cluster);
            $this->_clusterModel->rebuildClusterImage($cluster, $clusterImgPath);

            $lat = $cluster->latitude;
            $lng = $cluster->longitude;
            $level--;
        }while($level >= 0);
    }

    protected function _addSpots($eventId, $spots)
    {
        $spotModel = new Spots();
        $spotModel->setEventId($eventId);
        foreach ($spots as $binomialName => $data) {
            $spec = $this->getOne('SELECT * FROM species WHERE binomial_name = "'.$binomialName.'"');
            if($spec){
                $spotModel->setSpecies($spec);

                foreach ($data as $color => $bands) {
                    foreach ($bands as $band => $num){
                        $spotModel->addSpot([
                            'colour' => $color,
                            'bands' => $band
                        ], $num, $this->_userId);
                    }
                }
            }
        }
    }

    protected function _validatePoint($data)
    {
        $result = [];
        if(empty($data['name'])){
            throw new \Exception('Name is required!');
        }
        $result['name'] = $data['name'];

        $result['comment'] = @$data['comment'];
        if(empty($data['date'])){
            throw new \Exception('Sample date is required!');
        }
        $result['date'] = $data['date'];
        if(empty($data['zoom'])){
            throw new \Exception('Zoom level is required!');
        }
        $result['zoom'] = $data['zoom'];
        if(empty($data['position']['lat']) || empty($data['position']['lng'])){
            throw new \Exception('Location details is required!');
        }
        $result['position'] = $data['position'];
        if(empty($data['habitat']) || empty($data['habitat'])){
            throw new \Exception('Habitat details is required!');
        }
        $result['habitat'] = $data['habitat'];

        $result['spots'] = [];
        foreach (['nemoralis','hortensis'] as $bName){
            if(isset($data['samples'][$bName])){
                $binomialName = $this->_binomialName[$bName];
                $result['spots'][$binomialName] = [];
                foreach ($data['samples'][$bName] as $color => $bands){
                    if(!in_array($color, ['yellow','pink','brown']))
                        throw new \Exception('Color: '.$color.' is not supported');
                    if(array_sum($bands) == 0)
                        continue;
                    $result['spots'][$binomialName][$color] = [];

                    foreach ($bands as $band => $val){
                        if($val > 100)
                            throw new \Exception('You can\'t add more than 100 samples per snail type.');

                        if($val > 0)
                            $result['spots'][$binomialName][$color][ltrim($band, 'b')] = $val;
                    }
                }
            }
        }

        return $result;
    }
}